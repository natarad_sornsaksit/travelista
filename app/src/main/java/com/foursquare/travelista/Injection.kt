/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foursquare.travelista

import android.content.Context


/**
 * Enables injection of production implementations for
 * [TasksDataSource] at compile time.
 */
object Injection {

//    fun provideTasksRepository(context: Context): TasksRepository {
//        val database = ToDoDatabase.getInstance(context)
//        return TasksRepository.getInstance(
//            TasksRemoteDataSource,
//            TasksLocalDataSource.getInstance(AppExecutors(), database.taskDao())
//        )
//    }
//
//
//    fun providePatientsRepository(context: Context): PatientsRepository {
//        val database = DrCloudDatabase.getInstance(context)
//        return PatientsRepository.getInstance(
//            PatientsRemoteDataSource,
//            PatientsLocalDataSource.getInstance(AppExecutors(), database.patientDao())
//        )
//    }
//
//    fun provideDoctorsRepository(context: Context): DoctorsRepository {
//        val database = DrCloudDatabase.getInstance(context)
//        return DoctorsRepository.getInstance(
//            DoctorsRemoteDataSource,
//            DoctorsLocalDataSource.getInstance(AppExecutors(), database.doctorDao())
//        )
//    }
//
//    fun provideReportsRepository(context: Context): ReportsRepository {
//        val database = DrCloudDatabase.getInstance(context)
//        return ReportsRepository.getInstance(
//            ReportsRemoteDataSource,
//            ReportsLocalDataSource.getInstance(AppExecutors(), database.reportDao())
//        )
//    }
//
//    fun provideFormsRepository(context: Context): FormsRepository {
//        val database = DrCloudDatabase.getInstance(context)
//        return FormsRepository.getInstance(
//            FormsRemoteDataSource,
//            FormsLocalDataSource.getInstance(AppExecutors(), database.formDao())
//        )
//    }
}
