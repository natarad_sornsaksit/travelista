/*
 *  Copyright 2017 Google Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.foursquare.travelista

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.annotation.VisibleForTesting
import com.foursquare.travelista.ui.createtrip.CreateTripInviteViewModel
import com.foursquare.travelista.ui.createtrip.CreateTripViewModel
import com.foursquare.travelista.ui.generate.GenerateViewModel
import com.foursquare.travelista.ui.invitefriends.InviteFriendsViewModel
import com.foursquare.travelista.ui.login.LoginViewModel
import com.foursquare.travelista.ui.main.MainTripDetailViewModel
import com.foursquare.travelista.ui.main.MainViewModel
import com.foursquare.travelista.ui.mytrip.EditMyTripViewModel
import com.foursquare.travelista.ui.mytrip.MyTripViewModel
import com.foursquare.travelista.ui.notification.NotificationViewModel
import com.foursquare.travelista.ui.profile.ProfileViewModel
import com.foursquare.travelista.ui.recommendtrip.RecommendViewModel
import com.foursquare.travelista.ui.signin.SignInViewModel
import com.foursquare.travelista.ui.signup.SignUpViewModel

/**
 * A creator is used to inject the product ID into the ViewModel
 *
 *
 * This creator is to showcase how to inject dependencies into ViewModels. It's not
 * actually necessary in this case, as the product ID can be passed in a public method.
 */
class ViewModelFactory private constructor(
    private val application: Application
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) =
        with(modelClass) {
            when {
                isAssignableFrom(MainViewModel::class.java) ->
                    MainViewModel(application)
                isAssignableFrom(MainTripDetailViewModel::class.java) ->
                    MainTripDetailViewModel(application)
                isAssignableFrom(LoginViewModel::class.java) ->
                    LoginViewModel(application)
                isAssignableFrom(GenerateViewModel::class.java) ->
                    GenerateViewModel(application)
                isAssignableFrom(RecommendViewModel::class.java) ->
                    RecommendViewModel(application)
                isAssignableFrom(RecommendViewModel::class.java) ->
                    RecommendViewModel(application)
                isAssignableFrom(SignInViewModel::class.java) ->
                    SignInViewModel(application)
                isAssignableFrom(SignUpViewModel::class.java) ->
                    SignUpViewModel(application)
                isAssignableFrom(ProfileViewModel::class.java) ->
                    ProfileViewModel(application)
                isAssignableFrom(NotificationViewModel::class.java) ->
                    NotificationViewModel(application)
                isAssignableFrom(InviteFriendsViewModel::class.java) ->
                    InviteFriendsViewModel(application)
                isAssignableFrom(MyTripViewModel::class.java) ->
                    MyTripViewModel(application)
                isAssignableFrom(CreateTripViewModel::class.java) ->
                    CreateTripViewModel(application)
                isAssignableFrom(CreateTripInviteViewModel::class.java) ->
                    CreateTripInviteViewModel(application)
                isAssignableFrom(EditMyTripViewModel::class.java) ->
                    EditMyTripViewModel(application)
//                isAssignableFrom(HistoryViewModel::class.java) ->
//                    HistoryViewModel(application, patientsRepository, formsRepository)
//                isAssignableFrom(DoctorViewModel::class.java) ->
//                    DoctorViewModel(
//                        application,
//                        doctorsRepository,
//                        patientsRepository,
//                        formsRepository,
//                        reportsRepository
//                    )


                else ->
                    throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            }
        } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var INSTANCE: ViewModelFactory? = null

        fun getInstance(application: Application) =
            INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                INSTANCE ?: ViewModelFactory(
                    application
//                    Injection.providePatientsRepository(application.applicationContext),
//                    Injection.provideReportsRepository(application.applicationContext),
//                    Injection.provideFormsRepository(application.applicationContext),
//                    Injection.provideDoctorsRepository(application.applicationContext)
                )
                    .also { INSTANCE = it }
            }


        @VisibleForTesting
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
