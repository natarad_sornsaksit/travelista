package com.foursquare.travelista.data

import android.os.Parcel
import android.os.Parcelable
import com.foursquare.travelista.data.*
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.PropertyName
import paperparcel.PaperParcel
import paperparcel.PaperParcelable
import java.text.SimpleDateFormat
import java.util.*

@PaperParcel
data class Notification(

        @PropertyName("displayName")
        val displayName: String = "",
        @PropertyName("uid")
        val uid: String = "",
        @PropertyName("topic")
        val topic: String = "",
        @PropertyName("pictureUrl")
        val pictureUrl: String = "",
        @PropertyName("isRead")
        val isRead: Boolean = false,
        @PropertyName("type")
        val type: String = "",
        @PropertyName("timeStamp")
        val timeStamp: String = "" ,
        @PropertyName("tripID")
        val tripID: String = ""


) : Parcelable {
    companion object {
        @JvmField
        val CREATOR = PaperParcelNotification.CREATOR
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        PaperParcelNotification.writeToParcel(this, dest, flags)
    }

}