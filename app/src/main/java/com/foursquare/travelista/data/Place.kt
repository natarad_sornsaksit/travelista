package com.foursquare.travelista.data

import com.google.firebase.firestore.Exclude
import paperparcel.PaperParcel
import paperparcel.PaperParcelable
import java.util.*
import com.google.firebase.firestore.PropertyName
import com.google.firebase.firestore.GeoPoint

@PaperParcel
data class Place(
    @PropertyName("Budget")
    val budget: Int = 0,
    @PropertyName("Image")
    val image: String = "",
    @PropertyName("NameEN")
    val nameEN: String = "",
    @PropertyName("NameTH")
    val nameTH: String = "",
    @PropertyName("ProvinceEN")
    val provinceEN: String = "",
    @PropertyName("ProvinceTH")
    val provinceTH: String = "",
    @PropertyName("Style")
    val style: List<String> = emptyList(),
    @PropertyName("Type")
    val type: String = "",
    val Location: GeoPoint? = null
) : PaperParcelable {

    companion object {
        @JvmField
        val CREATOR = PaperParcelPlace.CREATOR
    }

    var id: String = ""
    var date: Date = Date()

    @Exclude
    fun toMap(): Map<String, Any> {
        val docData = HashMap<String, Any>()
        docData["Budget"] = budget
        docData["Image"] = image
        docData["NameEN"] = nameEN
        docData["NameTH"] = nameTH
        docData["ProvinceEN"] = provinceEN
        docData["ProvinceTH"] = provinceTH
        docData["Style"] = style
        docData["Type"] = type
        Location?.let {
            docData["Location"] = it
        }
        docData["updated_at"] = date
        return docData
    }
}
