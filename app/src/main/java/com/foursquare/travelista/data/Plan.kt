package com.foursquare.travelista.data

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.PropertyName
import paperparcel.PaperParcel
import java.text.SimpleDateFormat
import java.util.*

@PaperParcel
data class Plan(
        @PropertyName("ArrivedTime")
    var arrivedTime: Date = Date(),
        @PropertyName("Day")
    var day: Int = 0,
        @PropertyName("Place")
    var place: String = ""
) : Parcelable {
    companion object {
        @JvmField
        val CREATOR = PaperParcelPlan.CREATOR
    }

    var isShowHeader: Boolean = false
    @Exclude
    var id: String = ""


    @Exclude
    fun getTimeString(): String {
        val df = SimpleDateFormat("HH:mm")
        return df.format(arrivedTime)
    }


    @Exclude
    fun toMap(): Map<String, Any> {
        val docData = HashMap<String, Any>()
        docData["ArrivedTime"] = arrivedTime
        docData["Day"] = day
        docData["Place"] = place
        return docData
    }

    override fun describeContents() = 0
    override fun writeToParcel(dest: Parcel, flags: Int) {
        PaperParcelPlan.writeToParcel(this, dest, flags)

    }

}