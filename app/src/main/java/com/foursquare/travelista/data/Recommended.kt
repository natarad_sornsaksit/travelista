package com.foursquare.travelista.data

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.PropertyName
import paperparcel.PaperParcel

@PaperParcel
data class Recommended(
        @PropertyName("Name")
        val name: String = "",
        @PropertyName("PlanID")
        val planId: String = "",
        @PropertyName("Background")
        val background: String = "",
        @PropertyName("Province")
        val province: String = ""
) : Parcelable {
    companion object {
        @JvmField
        val CREATOR = PaperParcelRecommended.CREATOR
    }

    var budget: String = ""

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        PaperParcelRecommended.writeToParcel(this, dest, flags)
    }
}

