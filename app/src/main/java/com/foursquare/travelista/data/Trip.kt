package com.foursquare.travelista.data

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.Exclude
import paperparcel.PaperParcel
import java.text.SimpleDateFormat
import java.util.*
import com.google.firebase.firestore.PropertyName

@PaperParcel
data class Trip(
        @PropertyName("Name")
    var name: String = "",
        @PropertyName("PlanID")
    var planId: String = "",
        @PropertyName("Member")
    var member: Map<String, Boolean> = hashMapOf<String, Boolean>(),
        @PropertyName("Background")
    val background: String = "",
        @PropertyName("Province")
    var province: String = "",
        var startDate: Date = Date(),
        var endDate: Date = Date()
) : Parcelable {
    companion object {
        @JvmField
        val CREATOR = PaperParcelTrip.CREATOR
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        PaperParcelTrip.writeToParcel(this, dest, flags)
    }

    @Exclude
    var id: String = ""

    @Exclude
    fun getStartDateString(): String {
        val df = SimpleDateFormat("dd-MM-yyyy")
        return df.format(startDate)
    }


    @Exclude
    fun getEndDateString(): String {
        val dateFormat = SimpleDateFormat("dd-MM-yyyy")
        return dateFormat.format(endDate)
    }


    @Exclude
    fun toMap(): Map<String, Any> {
        val docData = HashMap<String, Any>()
        docData["Name"] = name
        docData["PlanID"] = planId
        docData["Member"] = member
        docData["Background"] = background
        docData["Province"] = province
        startDate?.let {
            docData["startDate"] = startDate
        }
        endDate?.let {
            docData["endDate"] = endDate
        }

        return docData
    }
}

