package com.foursquare.travelista.data

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.PropertyName
import paperparcel.PaperParcel

@PaperParcel
data class UserCalendar (

        @PropertyName("uid")
        val uid: String = "",
        @PropertyName("busyDay")
        val busyDay: List<Long> = emptyList()

): Parcelable {
    companion object {
        @JvmField
        val CREATOR = PaperParcelUserCalendar.CREATOR
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        PaperParcelUserCalendar.writeToParcel(this, dest, flags)
    }

}
