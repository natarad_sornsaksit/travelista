package com.foursquare.travelista.data

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.PropertyName
import paperparcel.PaperParcel

@PaperParcel
class Users(

    @PropertyName("displayName")
    val displayName: String = "",
    @PropertyName("uid")
    val uid: String = "",
    @PropertyName("email")
    val email: String = "",
    @PropertyName("photoUrl")
    val photoUrl: String = ""
) : Parcelable {
    companion object {
        @JvmField
        val CREATOR = PaperParcelUsers.CREATOR
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        PaperParcelUsers.writeToParcel(this, dest, flags)
    }

    @Exclude
    var isCheck: Boolean = false

}