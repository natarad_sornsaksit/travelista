package com.foursquare.travelista.ui.calendar

import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.provider.CalendarContract
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.foursquare.travelista.R
import com.foursquare.travelista.databinding.ActivityCalendarBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener




class CalendarActivity : AppCompatActivity() {
    private lateinit var viewDataBinding: ActivityCalendarBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_calendar)
        //recommendViewModel = obtainViewModel()
        viewDataBinding.apply {
            //            viewmodel = recommendViewModel
        }

        checkPermissionCalendar()
        getDataFromEventTable()
    }

    private fun checkPermissionCalendar() {
        Dexter.withActivity(this)
                .withPermission(android.Manifest.permission.READ_CALENDAR)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                        getDataFromEventTable()

                    }

                    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {

                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse?) {

                    }
                })
    }

    fun end(view: View){

        finish()

    }


    fun goToAccount(view: View){


        val addAccountIntent = Intent(android.provider.Settings.ACTION_ADD_ACCOUNT)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        //addAccountIntent.putExtra(Settings.EXTRA_ACCOUNT_TYPES)
        startActivity(addAccountIntent)


    }


    fun getDataFromEventTable() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.READ_CALENDAR),
                Companion.MY_CAL_REQ
            )
        }
        var cur: Cursor? = null
        val cr = contentResolver

        val mProjection = arrayOf("_id", CalendarContract.Events.AVAILABILITY, CalendarContract.Events.TITLE, CalendarContract.Events.EVENT_LOCATION, CalendarContract.Events.DTSTART, CalendarContract.Events.DTEND)

        val uri = CalendarContract.Events.CONTENT_URI
        //val selection = CalendarContract.Events.EV + " = ? "

        cur = cr.query(uri, mProjection, null, null, null)

        val disableDays = mutableSetOf<Long>()
        val busyDays = arrayListOf<Long>()


        while (cur!!.moveToNext()) {
            val title = cur.getString(cur.getColumnIndex(CalendarContract.Events.DTSTART))
            //Log.i("event", "" + cur.getInt(cur.getColumnIndex(CalendarContract.Events.AVAILABILITY)) + " " + cur.getString(cur.getColumnIndex(CalendarContract.Events.TITLE)) + " " + cur.getString(cur.getColumnIndex(CalendarContract.Events.DTEND)))
            disableDays.add(title.toLong())
            busyDays.add(title.toLong())

        }


        viewDataBinding.calendarView.disabledDays = disableDays
        //Log.i("Disable Day",""+disableDays)
        syncUsertoDataBase(busyDays)

    }

    private fun syncUsertoDataBase(busyDays: ArrayList<Long>) {

        var db = FirebaseFirestore.getInstance()
        var currentUser = FirebaseAuth.getInstance().currentUser

        //var calendar ;
        val calendar = HashMap<String, ArrayList<Long>>()

        calendar.put("busyDay",busyDays)

        // Add a new document with a generated ID
        db.collection("Calendar").document(currentUser!!.uid)
                .set(calendar as Map<String, Any>)
                .addOnSuccessListener { Log.d("", "DocumentSnapshot successfully written!") }
                .addOnFailureListener { e -> Log.w("", "Error writing document", e) }

    }

    companion object {
        private const val MY_CAL_REQ: Int = 1234
    }

}
