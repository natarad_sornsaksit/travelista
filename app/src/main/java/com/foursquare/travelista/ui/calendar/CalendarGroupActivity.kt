package com.foursquare.travelista.ui.calendar

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.foursquare.travelista.R
import com.foursquare.travelista.databinding.ActivityCalendarBinding
import com.google.firebase.firestore.FirebaseFirestore




class CalendarGroupActivity : AppCompatActivity() {
    private lateinit var viewDataBinding: ActivityCalendarBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_calendar)
        //recommendViewModel = obtainViewModel()
        viewDataBinding.apply {
            //            viewmodel = recommendViewModel
        }

        syncData()
    }

    fun end(view: View){

        finish()

    }


    fun goToAccount(view: View){


        val addAccountIntent = Intent(android.provider.Settings.ACTION_ADD_ACCOUNT)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        //addAccountIntent.putExtra(Settings.EXTRA_ACCOUNT_TYPES)
        startActivity(addAccountIntent)


    }

    fun syncData() {

        FirebaseFirestore.getInstance()
                .collection("Calendar").document("v3zd1RORDWZeYVPLvDZAqPjc21H2")
                .get()
                .addOnCompleteListener {
                    if (it.isComplete) {

                        var busyDays = listOf<Long>()

                        busyDays = it.result.data!!["busyDay"] as List<Long>

                        val disableDays = mutableSetOf<Long>()

                        for (i in 0 until busyDays.size) {
                            disableDays.add(busyDays[i])
                        }

                        getDataFromEventTable(disableDays)
                        Log.i("result ${it.isComplete}", disableDays.toString())

                    } else {
                        Log.e("isComplete ${it.isComplete}", it.exception.toString())
                    }
                }
                .addOnFailureListener {
                    Log.e("syncData", it.toString())
                }
    }

    private fun getDataFromEventTable(disableDays: MutableSet<Long>) {

        viewDataBinding.calendarView.disabledDays = disableDays
        Log.i("Disable Day",""+disableDays)

    }

}
