package com.foursquare.travelista.ui.createtrip

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.databinding.ItemCreateTripBinding
import java.util.*


class CreateTripAdapter(var data: ArrayList<Place>, var listener: OnItemClickListener) :
    RecyclerView.Adapter<CreateTripAdapter.TripViewHolder>() {


    interface OnItemClickListener {
        fun onItemClick(item: Place)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemCreateTripBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_create_trip, parent, false)
        return TripViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size

    fun addData(place: Place) {
        this.data.add(place)
        notifyDataSetChanged()
    }

    fun setData(tripList: List<Place>) {
        val sortList = tripList.sortedBy { it.date }
        this.data = ArrayList(sortList)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: TripViewHolder, position: Int) {
        holder.bind(data[position])
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.

    inner class TripViewHolder(val binding: ItemCreateTripBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Place) {
            binding.place = data
            binding.position =
                    when {
                        adapterPosition == 0 -> 1
                        adapterPosition == this@CreateTripAdapter.data.lastIndex && adapterPosition != 0 -> 3
                        else -> 2
                    }
            binding.executePendingBindings()
            itemView.setOnClickListener {
                listener.onItemClick(data)
            }
        }
    }

}