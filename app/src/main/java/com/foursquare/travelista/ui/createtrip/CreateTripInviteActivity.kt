package com.foursquare.travelista.ui.createtrip

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.data.Users
import com.foursquare.travelista.databinding.ActivityCreateTripInviteBinding
import com.foursquare.travelista.ui.main.MainActivity
import com.foursquare.travelista.util.obtainViewModel
import org.jetbrains.anko.*


class CreateTripInviteActivity : AppCompatActivity() {

    private lateinit var inviteViewModel: CreateTripInviteViewModel
    private lateinit var viewDataBinding: ActivityCreateTripInviteBinding
    private var viewAdapter: CreateTripInviteAdapter? = null
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_trip_invite)
        inviteViewModel = obtainViewModel().apply {
            if (intent.extras != null) {
                placeList = intent.getParcelableArrayListExtra("places")
                planList = intent.getSerializableExtra("plans") as MutableMap<String, Plan>
                trip = intent.getParcelableExtra("trip")

                Log.i("list place", placeList.toString())
                Log.i("list plan", planList.toString())
                Log.i("trip", trip.toString())
            }
        }
        viewDataBinding.apply {
            viewmodel = inviteViewModel
        }

        subscribeData()
        setList()
        inviteViewModel.syncData()

        viewDataBinding.btnCreateTrip.setOnClickListener {
            showDialogRequireUpload()
        }
    }


    private fun showDialogRequireUpload() {
        alert("Are you sure to create trip?") {
            yesButton {
                inviteViewModel.uploadData()
                it.dismiss()
            }
            noButton {
                it.dismiss()
            }
        }.show()
    }

    private fun subscribeData() {
        inviteViewModel.apply {
            userList.observe(this@CreateTripInviteActivity, android.arch.lifecycle.Observer {
                it?.let {
                    if (it.isNotEmpty()) {
                        viewAdapter!!.setData(it)
                        isShowCreateTrip.set(it.filter { it.isCheck }.isNotEmpty())
                    }
                }

            })

            isComplete.observe(this@CreateTripInviteActivity, Observer {
                it?.let {
                    goMain()
                }
            })
        }
    }

    fun goMain() {
        startActivity(intentFor<MainActivity>().clearTask().clearTop())
    }


    fun end(view: View?) {
        finish()
    }

    private fun setList() {

        viewManager = LinearLayoutManager(this)
        viewAdapter = CreateTripInviteAdapter(
                arrayListOf(),
                object : CreateTripInviteAdapter.OnItemClickListener {
                    override fun onItemClick(item: Users) {
                        inviteViewModel.setUserCheck(item)
                    }
                })

        viewDataBinding.rvFriend.apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }
    }

    private fun obtainViewModel(): CreateTripInviteViewModel =
            obtainViewModel(CreateTripInviteViewModel::class.java)
}
