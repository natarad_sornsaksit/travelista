package com.foursquare.travelista.ui.createtrip

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Users
import com.foursquare.travelista.databinding.ItemFriendInviteBinding
import java.util.*


class CreateTripInviteAdapter(
        var data: ArrayList<Users>,
        var listener: OnItemClickListener
) :
    RecyclerView.Adapter<CreateTripInviteAdapter.InviteViewHolder>() {


    interface OnItemClickListener {
        fun onItemClick(item: Users)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InviteViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemFriendInviteBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_friend_invite, parent, false)

        return InviteViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size

    fun setData(userList: List<Users>) {
        this.data = ArrayList(userList)
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {
        return data[position].uid.hashCode().toLong()
    }

    override fun onBindViewHolder(holder: InviteViewHolder, position: Int) {
        holder.bind(data[position])
    }


    inner class InviteViewHolder(val binding: ItemFriendInviteBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Users) {
            binding.users = data
            binding.executePendingBindings()
            Glide.with(itemView)
                .load(data.photoUrl)
                .apply(RequestOptions.circleCropTransform())
                .into(binding.ivProfile)

            binding.root.setOnClickListener {
                listener.onItemClick(data)
            }
        }
    }


}