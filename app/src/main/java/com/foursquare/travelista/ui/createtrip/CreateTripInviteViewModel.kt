/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foursquare.travelista.ui.createtrip

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableBoolean
import android.util.Log
import com.foursquare.travelista.SingleLiveEvent
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.data.Trip
import com.foursquare.travelista.data.Users
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*


class CreateTripInviteViewModel(
    context: Application
) : AndroidViewModel(context) {

    val userList = MutableLiveData<List<Users>>()

    val user = MutableLiveData<Users>()
    var placeList = ArrayList<Place>()
    var planList = mutableMapOf<String, Plan>()
    var trip: Trip? = null

    val isShowCreateTrip = ObservableBoolean(false)
    val isLoading = ObservableBoolean(false)
    val isComplete = SingleLiveEvent<Boolean>()

    fun syncData() {
        Log.i("syncData", "run")
        FirebaseFirestore.getInstance()
            .collection("Users")
            .get()
            .addOnCompleteListener {
                if (it.isComplete) {
                    val users = mutableListOf<Users>()
                    it.result.forEach {
                        val user = it.toObject(Users::class.java)
                        Log.i("Users", it.data.toString())
                        users.add(user)
                    }

                    if (users.isNotEmpty()) {
                        userList.postValue(users)
                    }

                } else {
                    Log.e("isComplete ${it.isComplete}", it.exception.toString())
                }
            }
            .addOnFailureListener {
                Log.e("syncData", it.toString())
            }
    }

    fun uploadData() {
        isLoading.set(true)
        val planId = FirebaseFirestore.getInstance().collection("Plans").document().id
        val usersSelected = userList.value!!.filter { it.isCheck }
        val members = hashMapOf<String, Boolean>()
        val plan: MutableMap<String, Any> = mutableMapOf()
        val placeMap: MutableMap<String, Any> = mutableMapOf()
        val taskCollection = arrayListOf<Task<Void>>()

        usersSelected.forEach {
            members[it.uid] = true
        }

        trip.apply {
            this!!.planId = planId
            this.member = members
        }

        placeList.forEach {
            placeMap[it.id] = it.toMap()
        }

        planList.forEach {
            plan[it.key] = it.value.toMap()
        }

        placeMap.forEach {
            taskCollection.add(FirebaseFirestore.getInstance()
                .collection("Places")
                .document(it.key)
                .set(it.value)
                .addOnFailureListener {
                    Log.e("placesTask", it.toString())
                }
            )
        }

        plan.forEach {
            taskCollection.add(
                FirebaseFirestore.getInstance()
                    .collection("Plans")
                    .document(planId)
                    .collection("Activity")
                    .document(it.key)
                    .set(it.value)
                    .addOnFailureListener {
                        Log.e("planTask", it.toString())
                    }
            )
        }


        val taskUploadTrip = FirebaseFirestore.getInstance()
            .collection("Trips")
            .add(trip!!.toMap())
            .addOnFailureListener {
                Log.e("tripTask", it.toString())
            }.addOnCompleteListener {
                Log.i("UploadData", "onCompleted")
                isLoading.set(false)
                if (it.isComplete) {
                    isComplete.postValue(true)

                } else {
                    isComplete.postValue(false)
                }
            }
        Tasks.whenAllComplete(taskCollection)
            .addOnFailureListener {
                Log.e("UploadData", it.toString())
            }.continueWith {
                taskUploadTrip
            }


    }


    fun setUserCheck(users: Users) {
        val index = userList.value!!.indexOf(users)
        userList.postValue(userList.value!!.apply {
            this[index].isCheck = !this[index].isCheck
        })
    }


}
