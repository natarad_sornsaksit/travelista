package com.foursquare.travelista.ui.createtrip

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.location.Location
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.View
import com.foursquare.travelista.R
import com.foursquare.travelista.custom.datetimepicker.SlideDateTimeListener
import com.foursquare.travelista.custom.datetimepicker.SlideDateTimePicker
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.databinding.ActivityCreateMapSearchBinding
import com.foursquare.travelista.util.obtainViewModel
import com.google.android.gms.location.places.Places
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.bottomsheet_create_trip_map.view.*
import org.jetbrains.anko.startActivity
import java.util.*


class CreateTripMapActivity : AppCompatActivity(), GoogleMap.OnMarkerClickListener,
    ActivityCompat.OnRequestPermissionsResultCallback, OnMapReadyCallback {
    private lateinit var mMap: GoogleMap
    private var mLocationPermissionGranted: Boolean = false
    private var mLastKnownLocation: Location? = null
    private lateinit var mAdapter: PlaceAutocompleteAdapter
    private lateinit var viewDataBinding: ActivityCreateMapSearchBinding
    private lateinit var mapsViewModel: CreateTripViewModel
    private lateinit var tripAdapter: CreateTripAdapter
    private lateinit var sheetBehavior: BottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_map_search)
        mapsViewModel = obtainViewModel().apply {
            if (intent.extras != null) {
                trip = intent.getParcelableExtra("trip")
            }
        }
        viewDataBinding.apply {
            viewmodel = mapsViewModel
        }

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)
        setupBottomSheet()
        setupAutoCompletePlace()
        setupPlaceList()
        observeViewModel(mapsViewModel)
    }

    private fun setupBottomSheet() {
        sheetBehavior = BottomSheetBehavior.from(viewDataBinding.bottomViewLayout)
        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                when (slideOffset.toInt()) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {

                    }
                }
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
            }
        })
    }

    private fun observeViewModel(mapViewModel: CreateTripViewModel) {
        mapViewModel.apply {
            listPlace.observe(this@CreateTripMapActivity, Observer {
                it?.let {
                    if (it.isNotEmpty()) {
                        tripAdapter.setData(it)
                        addMarker(it)

                        Log.i("list place", it.toString())
                        Log.i("list plan", mapsViewModel.planList.values.toString())

                    }
                    isShowBottomList.set(it.size > 0)
                }
            })
        }


        viewDataBinding.btnNext.setOnClickListener {
            goInvite()
        }
    }

    private fun goInvite() {
        val listPlace = mapsViewModel.listPlace.value
        val listPlan = mapsViewModel.planList
        val trip = mapsViewModel.trip
        startActivity<CreateTripInviteActivity>(
            "places" to listPlace,
            "plans" to listPlan,
            "trip" to trip
        )
    }


    private fun obtainViewModel(): CreateTripViewModel =
        obtainViewModel(CreateTripViewModel::class.java)

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        getLocationPermission()
        googleMap.setOnMapClickListener { latLng ->
            openPlacePicker(latLng)

//            val addresses: List<Address>
//            val geocoder = Geocoder(this, Locale.getDefault())
//            addresses = geocoder.getFromLocation(lat, lng, 1)
//
//            mMap.addMarker(MarkerOptions()
//                    .Location(LatLng(lat, lng))
//                    .title(addresses[0].adminArea))

            ; // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            //Log.i("onMarkerClick", addresses[0].getAddressLine(0))

        }

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(13.736717, 100.523186), 5F))

    }


    private fun setupAutoCompletePlace() {
        val mGeoDataClient = Places.getGeoDataClient(this)
        val BOUNDS_GREATER_SYDNEY = LatLngBounds(
            LatLng(95.951017, 20.643531), LatLng(106.190874, 4.996015)
        )
        mAdapter = PlaceAutocompleteAdapter(this, mGeoDataClient, BOUNDS_GREATER_SYDNEY, null)
        viewDataBinding.tvSearchPlace.setAdapter(mAdapter)
        viewDataBinding.tvSearchPlace.setOnItemClickListener { adapterView, view, i, l ->

            val item = mAdapter.getItem(i)
            val placeId = item.placeId

            val placeResult = mGeoDataClient.getPlaceById(placeId!!)
            placeResult.addOnCompleteListener {
                try {
                    val places = it.result
                    // Get the Place object from the buffer.
                    val place = places.get(0)
                    moveCamera(place.latLng, 16F)
                    // Format details of the place for display and show it in a TextView.

//                    mMap.addMarker(MarkerOptions()
//                            .Location(place.latLng))
                    places.release()
                } catch (e: RuntimeRemoteException) {
                    // Request did not complete successfully
                    Log.e("placeResult", "Place query did not complete.", e)
                }

            }
        }
    }

    private fun moveCamera(latLng: LatLng) {
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, mMap.cameraPosition.zoom)
        // Add a marker in Sydney, Australia, and move the camera.
        mMap.animateCamera(cameraUpdate)
    }

    private fun moveCamera(latLng: LatLng, zoom: Float) {
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom)
        // Add a marker in Sydney, Australia, and move the camera.
        mMap.animateCamera(cameraUpdate)
    }

    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(
                this.applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }


    private fun updateLocationUI() {
        if (mMap == null) {
            return
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.isMyLocationEnabled = true
                mMap.uiSettings.isMyLocationButtonEnabled = true
            } else {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings.isMyLocationButtonEnabled = false
                mLastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message)
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        mLocationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true
                }
            }
        }
        updateLocationUI()
    }


    override fun onMarkerClick(marker: Marker): Boolean {

        return false
    }


    override fun onPointerCaptureChanged(hasCapture: Boolean) {

    }

    companion object {
        const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 7
        private const val PLACE_PICKER_REQUEST = 1
    }

    private fun openPlacePicker(latLng: LatLng) {
        val builder = PlacePicker.IntentBuilder()
        val rangN = LatLng(latLng.latitude + 0.005, latLng.longitude + 0.005)
        val rangS = LatLng(latLng.latitude - 0.005, latLng.longitude - 0.005)

        //set start point
        val bounds: LatLngBounds = LatLngBounds.Builder()
            .include(rangN)
            .include(rangS).build()

        builder.setLatLngBounds(bounds)
        startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                val place = PlacePicker.getPlace(this, data)
                createPlan(place)
            }
        }
    }


    private fun createPlan(place: com.google.android.gms.location.places.Place) {
        SlideDateTimePicker.Builder(supportFragmentManager)
            .setListener(object : SlideDateTimeListener() {
                override fun onDateTimeSet(date: Date?) {
                    date?.let {
                        Log.i("onDateTimeSet", date.toString())
                        Log.i("place", place.toString())
                        mapsViewModel.addPlaceAndPlan(place, date)
                        moveCamera(place.latLng)
                    }
                }
            })
            .setMinDate(mapsViewModel.trip?.startDate)
            .setMaxDate(mapsViewModel.trip?.endDate)
            .setInitialDate(Date())
            .setIs24HourTime(true)
            .build()
            .show()
    }

    private fun setupPlaceList() {
        val linearLayoutManager = LinearLayoutManager(this)
        tripAdapter =
                CreateTripAdapter(arrayListOf(), object : CreateTripAdapter.OnItemClickListener {
                    override fun onItemClick(item: Place) {
                        item.Location?.let {
                            moveCamera(LatLng(it.latitude, it.longitude))
                        }
                    }
                })

        viewDataBinding.bottomViewLayout.recycler_view.apply {
            layoutManager = linearLayoutManager
            adapter = tripAdapter
            val touchHelper = object :
                ItemTouchHelper.SimpleCallback(
                    0,
                    ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
                ) {
                override fun onMove(
                    recyclerView: RecyclerView?,
                    viewHolder: RecyclerView.ViewHolder?,
                    target: RecyclerView.ViewHolder?
                ): Boolean {
//                    val fromPos: Int = viewHolder!!.adapterPosition
//                    val toPos: Int = target!!.adapterPosition
//                    val place = mapsViewModel.listPlace.value!!.removeAt(fromPos)
//                    mapsViewModel.listPlace.value!!.add(
//                        when {
//                            toPos > fromPos -> toPos - 1
//                            else -> toPos
//                        }, place
//                    )
//                    adapter.notifyItemMoved(fromPos, toPos);
//
//                    Log.i("onMove", "$fromPos $toPos")
                    return false
                }


                override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
                    if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                        viewHolder?.let {
                            mapsViewModel.removePlace(it.adapterPosition)
                        }
                    }
                }

            }
            ItemTouchHelper(touchHelper).attachToRecyclerView(this)
        }
    }


    private fun addMarker(listPlace: ArrayList<Place>) {
        mMap.clear()
        listPlace.forEach { place ->
            place.Location?.let {
                mMap.addMarker(
                    MarkerOptions()
                        .position(LatLng(it.latitude, it.longitude))
                        .title(place.nameEN)
                )
            }
        }


    }


}
