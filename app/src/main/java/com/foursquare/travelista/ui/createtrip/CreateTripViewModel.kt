/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foursquare.travelista.ui.createtrip

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableBoolean
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.data.Trip
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import org.threeten.bp.*
import java.util.*


class CreateTripViewModel(
    context: Application
) : AndroidViewModel(context) {

    val listPlace = MutableLiveData<ArrayList<Place>>()
    val isShowBottomList = ObservableBoolean(false)
    val planList = mutableMapOf<String, Plan>()
    var trip: Trip? = null

    init {
        listPlace.value = arrayListOf()
    }

    private fun addPlan(place: Place, date: Date) {
        val startDate =
            LocalDateTime.ofInstant(
                Instant.ofEpochMilli(trip!!.startDate!!.time),
                ZoneId.systemDefault()
            )

        val targetDate = LocalDateTime.ofInstant(
            Instant.ofEpochMilli(date.time),
            ZoneId.systemDefault()
        )

        val day = Duration.between(startDate, targetDate).toDays().toInt() + 1

        planList[FirebaseFirestore.getInstance().collection("Plans").document().id] =
                Plan(place = place.id, arrivedTime = date, day = day)
    }

    fun removePlace(int: Int) {
        val keys = planList.filter {
            it.value.place == listPlace.value!![int].id
        }.keys
        keys.forEach {
            planList.remove(it)
        }

        listPlace.postValue(
            listPlace.value.apply {
                this!!.removeAt(int)
            }
        )
    }

    fun addPlaceAndPlan(place: com.google.android.gms.location.places.Place, date: Date) {
        val newPlace = Place(
                Location = GeoPoint(place.latLng.latitude, place.latLng.longitude),
                nameEN = place.name.toString(),
                nameTH = place.name.toString(),
                provinceEN = place.address.toString(),
                provinceTH = place.address.toString(),
                budget = 100
        ).apply {
            id = place.id
            this.date = date
        }

        addPlan(newPlace, date)

        listPlace.postValue(
            listPlace.value.apply {
                this!!.add(
                    newPlace
                )
            }
        )
    }

}
