package com.foursquare.travelista.ui.generate

import android.app.DatePickerDialog
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Trip
import com.foursquare.travelista.databinding.ActivityGenerateBinding
import com.foursquare.travelista.util.obtainViewModel
import com.foursquare.travelista.ui.createtrip.CreateTripMapActivity
import com.foursquare.travelista.ui.recommendtrip.RecommendTripActivity
import com.foursquare.travelista.util.showSnackbar
import org.jetbrains.anko.startActivity
import java.text.SimpleDateFormat
import java.util.*


class GenerateActivity : AppCompatActivity() {
    private lateinit var generateViewModel: GenerateViewModel
    private lateinit var viewDataBinding: ActivityGenerateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_generate)
        generateViewModel = obtainViewModel()
        viewDataBinding.apply {
            viewmodel = generateViewModel
        }
        setSupportActionBar(viewDataBinding.toolbar)

        viewDataBinding.btnGenerate.setOnClickListener {
            if (checkRequireField(generateViewModel.isShowGenerate.get())) {
                if (generateViewModel.isShowGenerate.get()) {
                    goRecommend()
                } else {
                    goCreate()
                }
            }
        }

        setupSpinner()
    }


    private fun setupSpinner() {
        val provinces = arrayListOf(
            "กระบี่",
            "กรุงเทพมหานคร",
            "กาญจนบุรี",
            "กาฬสินธุ์",
            "กำแพงเพชร",
            "ขอนแก่น",
            "จันทบุรี",
            "ฉะเชิงเทรา",
            "ชลบุรี",
            "ชัยนาท",
            "ชัยภูมิ",
            "ชุมพร",
            "เชียงราย",
            "เชียงใหม่",
            "ตรัง",
            "ตราด",
            "ตาก",
            "นครนายก",
            "นครปฐม",
            "นครพนม",
            "นครราชสีมา",
            "นครศรีธรรมราช",
            "นครสวรรค์",
            "นนทบุรี",
            "นราธิวาส",
            "น่าน",
            "บุรีรัมย์",
            "ปทุมธานี",
            "ประจวบคีรีขันธ์",
            "ปราจีนบุรี",
            "ปัตตานี",
            "พะเยา",
            "พังงา",
            "พัทลุง",
            "พิจิตร",
            "พิษณุโลก",
            "เพชรบุรี",
            "เพชรบูรณ์",
            "แพร่",
            "ภูเก็ต",
            "มหาสารคาม",
            "มุกดาหาร",
            "แม่ฮ่องสอน",
            "ยโสธร",
            "ยะลา",
            "ร้อยเอ็ด",
            "ระนอง",
            "ระยอง",
            "ราชบุรี",
            "ลพบุรี",
            "ลำปาง",
            "ลำพูน",
            "เลย",
            "ศรีสะเกษ",
            "สกลนคร",
            "สงขลา",
            "สตูล",
            "สมุทรปราการ",
            "สมุทรสงคราม",
            "สมุทรสาคร",
            "สระแก้ว",
            "สระบุรี",
            "สิงห์บุรี",
            "สุโขทัย",
            "สุพรรณบุรี",
            "สุราษฎร์ธานี",
            "สุรินทร์",
            "หนองคาย",
            "หนองบัวลำภู",
            "อยุธยา",
            "อ่างทอง",
            "อำนาจเจริญ",
            "อุดรธานี",
            "อุตรดิตถ์",
            "อุทัยธานี",
            "อุบลราชธานี"
        )
        val adapter = ArrayAdapter(
            this,
            android.R.layout.simple_dropdown_item_1line, provinces
        )
        viewDataBinding.etLocation.adapter = adapter
        viewDataBinding.etLocation.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View,
                        position: Int,
                        id: Long
                    ) {
                        val result = parent.getItemAtPosition(position) as String
                        generateViewModel.locationSelected = result
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {

                    }
                }


    }

    private fun checkRequireField(isGenerateMode: Boolean): Boolean {
        var isPass = true

        if (viewDataBinding.etTripName.text.isBlank()) {
            isPass = false
            viewDataBinding.etTripName.error = "This fields is required."
        }
        if (generateViewModel.locationSelected.isBlank()) {
            isPass = false
            viewDataBinding.etLocation.showSnackbar(
                "Please selected location.",
                Snackbar.LENGTH_SHORT
            )
        }
        if (viewDataBinding.etStartDate.text.isBlank()) {
            isPass = false
            viewDataBinding.etStartDate.error = "This fields is required."
        }
        if (viewDataBinding.etEndDate.text.isBlank()) {
            isPass = false
            viewDataBinding.etEndDate.error = "This fields is required."
        }

        if (isGenerateMode) {

        }

        return isPass
    }

    private fun obtainViewModel(): GenerateViewModel =
        obtainViewModel(GenerateViewModel::class.java)

    fun showDateEnd(view: View) {
        val myCalendar = Calendar.getInstance()
        val myFormat = "MM/dd/yy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        val startDateString = viewDataBinding.etStartDate.text.toString()
        val startDate = sdf.parse(startDateString)

        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            view as TextView
            if (viewDataBinding.etStartDate.text.isBlank()) {
                view.text = sdf.format(myCalendar.time)
                view.error = null
            } else {

                if (startDate.time > myCalendar.time.time) {
                    view.showSnackbar(
                        "Please select end date more than start date",
                        Snackbar.LENGTH_SHORT
                    )
                } else {
                    view.text = sdf.format(myCalendar.time)
                    view.error = null
                }
            }
        }
        val dp = DatePickerDialog(
            this@GenerateActivity, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
            myCalendar.get(Calendar.DAY_OF_MONTH)
        )
        if (sdf.parse(startDateString).time > 0) {
            dp.datePicker.minDate = sdf.parse(startDateString).time
        }
        dp.show()
    }

    fun showDateStart(view: View) {
        val myCalendar = Calendar.getInstance()
        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)


            val myFormat = "MM/dd/yy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            view as TextView
            if (viewDataBinding.etEndDate.text.isBlank()) {
                view.text = sdf.format(myCalendar.time)
                view.error = null
            } else {
                val endDateString = viewDataBinding.etEndDate.text.toString()
                val endDate = sdf.parse(endDateString)
                if (endDate.time < myCalendar.time.time) {
                    view.showSnackbar(
                        "Please select start date less than end date",
                        Snackbar.LENGTH_SHORT
                    )
                } else {
                    view.text = sdf.format(myCalendar.time)
                    view.error = null
                }
            }
        }
        val dp = DatePickerDialog(
            this@GenerateActivity, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
            myCalendar.get(Calendar.DAY_OF_MONTH)
        )
        dp.datePicker.minDate = myCalendar.timeInMillis
        dp.show()
    }

    private fun goRecommend() {
        startActivity(Intent(this@GenerateActivity, RecommendTripActivity::class.java))
    }

    private fun goCreate() {
        val myFormat = "MM/dd/yy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)

        val name = viewDataBinding.etTripName.text.toString()
        val location = generateViewModel.locationSelected
        val startDate: Date = sdf.parse(viewDataBinding.etStartDate.text.toString())
        val endDate: Date = sdf.parse(viewDataBinding.etEndDate.text.toString())

        val trip = Trip(name = name, province = location, startDate = startDate, endDate = endDate)

        startActivity<CreateTripMapActivity>("trip" to trip)
    }
}
