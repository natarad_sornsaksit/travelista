package com.foursquare.travelista.ui.interest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.foursquare.travelista.R;
import com.foursquare.travelista.ui.main.MainActivity;

public class InterestActivity extends Activity implements View.OnClickListener {

    ImageView ivCity , ivCitycover;
    ImageView ivCultral,ivCultralcover;
    ImageView ivHistorical,ivHistoricalcover;
    ImageView ivArt,ivArtcover;
    ImageView ivMountain,ivMountaincover;
    ImageView ivWaterfall,ivWaterfallcover;
    ImageView ivForest,ivForestcover;
    ImageView ivCave,ivCavecover;
    ImageView ivBeach,ivBeachcover;
    ImageView ivIsland,ivIslandcover;
    ImageView imageView3;
    TextView textView;
    ImageView imageView13;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interesting);

        ivCity = findViewById(R.id.ivCity);
        ivCitycover = findViewById(R.id.iv1);
        ivCultral = findViewById(R.id.ivCultral);
        ivCultralcover = findViewById(R.id.iv2);
        ivHistorical = findViewById(R.id.ivHistorical);
        ivHistoricalcover = findViewById(R.id.iv3);
        ivArt = findViewById(R.id.ivArt);
        ivArtcover = findViewById(R.id.iv4);
        ivMountain = findViewById(R.id.ivMountain);
        ivMountaincover = findViewById(R.id.iv5);
        ivWaterfall = findViewById(R.id.ivWaterfall);
        ivWaterfallcover = findViewById(R.id.iv6);
        ivForest = findViewById(R.id.ivForest);
        ivForestcover = findViewById(R.id.iv7);
        ivCave = findViewById(R.id.ivCave);
        ivCavecover = findViewById(R.id.iv8);
        ivBeach = findViewById(R.id.ivBeach);
        ivBeachcover = findViewById(R.id.iv9);
        ivIsland = findViewById(R.id.ivIsland);
        ivIslandcover = findViewById(R.id.iv10);

        ivCitycover.setOnClickListener(this);
        ivCultralcover.setOnClickListener(this);
        ivHistoricalcover.setOnClickListener(this);
        ivArtcover.setOnClickListener(this);
        ivMountaincover.setOnClickListener(this);
        ivWaterfallcover.setOnClickListener(this);
        ivForestcover.setOnClickListener(this);
        ivCavecover.setOnClickListener(this);
        ivBeachcover.setOnClickListener(this);
        ivIslandcover.setOnClickListener(this);

        imageView3 = findViewById(R.id.imageView3);
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        textView = findViewById(R.id.textView);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InterestActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

        imageView13 = findViewById(R.id.imageView13);
        imageView13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InterestActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv1:
                //action
                if(ivCity.getVisibility() == View.VISIBLE) {
                    ivCity.setVisibility(View.GONE);
                }else{
                    ivCity.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.iv2:
                //action
                if(ivCultral.getVisibility() == View.VISIBLE) {
                    ivCultral.setVisibility(View.GONE);
                }else{
                    ivCultral.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.iv3:
                //action
                if(ivHistorical.getVisibility() == View.VISIBLE) {
                    ivHistorical.setVisibility(View.GONE);
                }else{
                    ivHistorical.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.iv4:
                //action
                if(ivArt.getVisibility() == View.VISIBLE) {
                    ivArt.setVisibility(View.GONE);
                }else{
                    ivArt.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.iv5:
                //action
                if(ivMountain.getVisibility() == View.VISIBLE) {
                    ivMountain.setVisibility(View.GONE);
                }else{
                    ivMountain.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.iv6:
                //action
                if(ivWaterfall.getVisibility() == View.VISIBLE) {
                    ivWaterfall.setVisibility(View.GONE);
                }else{
                    ivWaterfall.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.iv7:
                //action
                if(ivForest.getVisibility() == View.VISIBLE) {
                    ivForest.setVisibility(View.GONE);
                }else{
                    ivForest.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.iv8:
                //action
                if(ivCave.getVisibility() == View.VISIBLE) {
                    ivCave.setVisibility(View.GONE);
                }else{
                    ivCave.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.iv9:
                //action
                if(ivBeach.getVisibility() == View.VISIBLE) {
                    ivBeach.setVisibility(View.GONE);
                }else{
                    ivBeach.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.iv10:
                //action
                if(ivIsland.getVisibility() == View.VISIBLE) {
                    ivIsland.setVisibility(View.GONE);
                }else{
                    ivIsland.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

}
