package com.foursquare.travelista.ui.invitefriends

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Users
import com.foursquare.travelista.databinding.ActivityInviteFriendsBinding
import com.foursquare.travelista.util.obtainViewModel
import org.jetbrains.anko.toast


class InviteFriendsActivity : AppCompatActivity() {

    private lateinit var inviteViewModel: InviteFriendsViewModel
    private lateinit var viewDataBinding: ActivityInviteFriendsBinding
    private var viewAdapter: inviteFriendAdapter? = null
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this,R.layout.activity_invite_friends)
        inviteViewModel = obtainViewModel()

        viewDataBinding.apply {
            viewmodel = inviteViewModel
        }
        subscribeData()
        setList()
        inviteViewModel.syncData()

    }

    private fun subscribeData() {
        inviteViewModel.apply {
            userList.observe(this@InviteFriendsActivity, android.arch.lifecycle.Observer {
                it?.let {
                    it?.let {
                        viewAdapter!!.setData(it)
                    }
                }

            })
        }
    }

    fun end(view: View?) {

        finish()

    }

    private fun setList() {

        viewManager = LinearLayoutManager(this)
        viewAdapter = inviteFriendAdapter(this,arrayListOf(), object : inviteFriendAdapter.OnItemClickListener {
            override fun onItemClick(item: Users) {
                toast(item.uid)
            }
        })

        recyclerView = findViewById<RecyclerView>(R.id.rvFriend).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }
    }

    private fun obtainViewModel(): InviteFriendsViewModel = obtainViewModel(InviteFriendsViewModel::class.java)
}
