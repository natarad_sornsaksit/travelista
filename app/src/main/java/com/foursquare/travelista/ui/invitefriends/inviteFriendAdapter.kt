package com.foursquare.travelista.ui.invitefriends

import android.content.Context
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Users
import com.foursquare.travelista.databinding.FriendRowBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*


class inviteFriendAdapter(context: Context, var data: ArrayList<Users>, var listener: OnItemClickListener) :
        RecyclerView.Adapter<inviteFriendAdapter.InviteViewHolder>() {

    private val context: Context
    var currentUser = FirebaseAuth.getInstance().currentUser


    init {

        this.context = context
    }



    interface OnItemClickListener {
        fun onItemClick(item: Users)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InviteViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding:FriendRowBinding = DataBindingUtil.inflate(layoutInflater, R.layout.friend_row, parent, false)

        return InviteViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size

    fun setData(userList: List<Users>) {
        this.data = ArrayList(userList)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: InviteViewHolder, position: Int) {
        holder.bind(data[position])
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.

    inner class InviteViewHolder(val binding: FriendRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Users) {
            binding.users = data
            binding.executePendingBindings()
            Glide.with(itemView)
                    .load(data.photoUrl)
                    .apply(RequestOptions.circleCropTransform())
                    .into(binding.ivProfile)
//            itemView.setOnClickListener {
//                listener.onItemClick(data)
//            }

            if (data.uid.equals(currentUser!!.uid)){
                itemView.visibility = View.GONE}

            binding.ivInvite.setOnClickListener(View.OnClickListener { v -> sendInvite(data)

                binding.ivInvite.setTextColor(Color.WHITE)
                binding.ivInvite.isClickable = false


            })

            binding.tvUsername.text = data.displayName
        }
    }

    private fun sendInvite(data: Users) {



        var db = FirebaseFirestore.getInstance()
        var currentUser = FirebaseAuth.getInstance().currentUser
        //var tokenUser  =  FirebaseInstanceId.getInstance().getToken()


        val user = HashMap<String, String>()
        user.put("displayName", currentUser!!.displayName.toString())
        user.put("topic", "Invite you to Trip")
        user.put("uid", currentUser?.uid)
        user.put("timeStamp", "min")
        user.put("pictureUrl", currentUser?.photoUrl.toString())
        user.put("tripID", "CsZzB5IH4WB8QKAofgpS")


        // Add a new document with a generated ID
        db.collection("Users/"+data.uid+"/notification")
                .add(user as Map<String, Any>)
                .addOnSuccessListener { Log.d("", "DocumentSnapshot successfully written!") }
                .addOnFailureListener { e -> Log.w("", "Error writing document", e) }

    }

}