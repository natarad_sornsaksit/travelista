package com.foursquare.travelista.ui.invitetrip

import android.arch.lifecycle.Observer
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.bumptech.glide.Glide
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Trip
import com.foursquare.travelista.databinding.ActivityTripBinding
import com.foursquare.travelista.ui.calendar.CalendarActivity
import com.foursquare.travelista.ui.mytrip.MyTripDetailActivity
import com.foursquare.travelista.ui.recommendtrip.ImageSliderAdapter
import com.foursquare.travelista.ui.recommendtrip.PlacesAdapter
import com.foursquare.travelista.ui.recommendtrip.RecommendViewModel
import com.foursquare.travelista.util.obtainViewModel
import org.jetbrains.anko.intentFor

class InviteTripActivity : AppCompatActivity() {

    private lateinit var recommendViewModel: RecommendViewModel
    private lateinit var viewDataBinding: ActivityTripBinding
    private var adapter: PlacesAdapter? = null
    private var sliderAdapter: ImageSliderAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_trip)
        recommendViewModel = obtainViewModel()
        viewDataBinding.apply {
            viewmodel = recommendViewModel
        }

        recommendViewModel.syncData()
        if (intent.extras != null) {
            val trip = intent.getParcelableExtra<Trip>("trip")
            recommendViewModel.trip.postValue(trip)
            recommendViewModel.sync(trip)
            viewDataBinding.trip = trip

            viewDataBinding.btnGoTrip.text = "Join Trip"
            viewDataBinding.btnGoTrip.setOnClickListener(View.OnClickListener { v -> goToMyTripDetail(trip) })
            viewDataBinding.textView19.text = trip.name
        }


        Glide.with(this)
                .load(R.drawable.w_calendar)
                .into(viewDataBinding.imageViewEdit)


        setupList()
        subscribeData(recommendViewModel)

        viewDataBinding.imageViewBack.setOnClickListener(View.OnClickListener { v -> finish() })


        viewDataBinding.imageViewEdit.setOnClickListener(View.OnClickListener { v -> ontoCalendar() })


    }

    private fun ontoCalendar() {

        startActivity(Intent(this@InviteTripActivity, CalendarActivity::class.java))

    }

    private fun obtainViewModel(): RecommendViewModel = obtainViewModel(RecommendViewModel::class.java)

    private fun subscribeData(recommendViewModel: RecommendViewModel) {
        recommendViewModel?.let {
            it.isCompleted.observe(this, Observer { it1: Boolean? ->
                it1?.let { it1: Boolean ->
                    if (it1) {
                        adapter!!.setData(it.planList.value, it.placeList.value)
                    }

                }
            })

            it.placeInPlan.observe(this, Observer {
                it?.let { setupImageSlider(it) }
            })
        }
    }

    private fun setupImageSlider(list: List<Place>) {
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        viewDataBinding.imageViewSlider.layoutManager = linearLayoutManager
        sliderAdapter = ImageSliderAdapter(ArrayList(list), object : ImageSliderAdapter.OnItemClickListener {
            override fun onItemClick(item: Place) {
            }
        })
        viewDataBinding.imageViewSlider.adapter = sliderAdapter
    }

    private fun setupList() {
        val linearLayoutManager = LinearLayoutManager(this)
        viewDataBinding.recycleView1.layoutManager = linearLayoutManager

        adapter = PlacesAdapter(arrayListOf(), arrayListOf(), object : PlacesAdapter.OnItemClickListener {
            override fun onItemClick(item: Place) {
            }
        })
        viewDataBinding.recycleView1.adapter = adapter
    }

    fun goToMyTripDetail(trip: Trip) {
        startActivity(intentFor<MyTripDetailActivity>("trip" to trip))
        finish()
    }
}

