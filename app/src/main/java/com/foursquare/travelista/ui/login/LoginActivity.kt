package com.foursquare.travelista.ui.login

import android.app.ProgressDialog
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.foursquare.travelista.R
import com.foursquare.travelista.databinding.ActivityLoginBinding
import com.foursquare.travelista.ui.interest.InterestActivity
import com.foursquare.travelista.ui.signin.SignInActivity
import com.foursquare.travelista.util.obtainViewModel
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginResult
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId


class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var viewDataBinding: ActivityLoginBinding
    private var Dialog: ProgressDialog? = null


    private var mAuth: FirebaseAuth? = null
    private var mAuthListener: FirebaseAuth.AuthStateListener? = null
    private var mCallbackManager: CallbackManager? = null

    private var TAG = "LoginActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppEventsLogger.activateApp(this)
        mAuth = FirebaseAuth.getInstance()
        mCallbackManager = CallbackManager.Factory.create()
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        loginViewModel = obtainViewModel()
        viewDataBinding.apply { viewmodel = loginViewModel }

        initInstace()

    }

    override fun onStart() {
        super.onStart()

        //val currentUser = mAuth?.getCurrentUser()

        mAuthListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user != null) {
                Log.d(TAG, "onAuthStateChanged:signed_in:" + user.uid)
                syncUsertoDataBase()
                StartMain()
                finish()

            } else {
                Log.d(TAG, "onAuthStateChanged:signed_out")
            }
        }

        mAuth?.addAuthStateListener(mAuthListener!!)




    }

    private fun StartMain() {

//        startActivity(Intent(this@LoginActivity, MainActivity::class.java))
        startActivity(Intent(this@LoginActivity, InterestActivity::class.java))

    }


    private fun initInstace() {

        viewDataBinding.connectWithFbButton.setReadPermissions("email", "public_profile")

        viewDataBinding.connectWithFbButton.registerCallback(mCallbackManager, object : FacebookCallback<LoginResult> {

            override fun onSuccess(result: LoginResult) {

                Log.d(TAG, "facebook:onSuccess:" + result)

                handleFacebookAccessToken(result.accessToken)

            }

            override fun onCancel() {
                //TODO Auto-generated method stub

                Log.d(TAG, "facebook:onCancel")
            }

            override fun onError(error: FacebookException) {
                //TODO Auto-generated method stub
                Log.d(TAG, "facebook:onError", error)
            }
        })

        viewDataBinding.btEmailLogin.setOnClickListener(View.OnClickListener { v -> onClick(v) })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        mCallbackManager!!.onActivityResult(requestCode, resultCode, data)

    }

    private fun handleFacebookAccessToken(token: AccessToken) {

        Dialog = ProgressDialog(this)
        Dialog!!.setMessage("Loading...")
        Dialog!!.show()
        //Log.d(TAG, "Token is :"+ token.token)
        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->

                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success")

                        syncUsertoDataBase()

                        Dialog!!.dismiss()
                        StartMain()

                        //val user = firebaseAuth!!.currentUser
                        //startActivity(Intent(this@CreateAccount, MainActivity::class.java))

                    }

                    Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful)

                    // If sign in fails, display a message to the user. If sign in succeeds
                    // the auth state listener will be notified and logic to handle the
                    // signed in user can be handled in the listener.
                    if (!task.isSuccessful) {
                        Log.w(TAG, "signInWithCredential", task.exception)
                        Dialog!!.dismiss()
                        Toast.makeText(this@LoginActivity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                    }

                }
    }

    private fun syncUsertoDataBase() {

       var db = FirebaseFirestore.getInstance()
       var currentUser = FirebaseAuth.getInstance().currentUser
       var tokenUser  =  FirebaseInstanceId.getInstance().getToken()

        val user = HashMap<String, String>()
        user.put("displayName", currentUser!!.displayName.toString())
        user.put("uid", currentUser?.uid)
        user.put("email", currentUser?.email.toString())
        user.put("photoUrl", currentUser?.photoUrl.toString())
        user.put("tokenId", tokenUser.toString())


        // Add a new document with a generated ID
        db.collection("Users").document(currentUser!!.uid)
                .set(user as Map<String, Any>)
                .addOnSuccessListener { Log.d("", "DocumentSnapshot successfully written!") }
                .addOnFailureListener { e -> Log.w("", "Error writing document", e) }

    }

    fun onClick(v: View) {
        if (v === viewDataBinding.fb) {
            viewDataBinding.connectWithFbButton.performClick()
        }
        if (v === viewDataBinding.btEmailLogin) {

            val intent = Intent(applicationContext, SignInActivity::class.java)
            // val message = editText_main!!.text.toString()
            // intent.putExtra(EXTRA_MESSAGE, message)
            startActivity(intent)
        }

    }


    private fun obtainViewModel(): LoginViewModel = obtainViewModel(LoginViewModel::class.java)


}