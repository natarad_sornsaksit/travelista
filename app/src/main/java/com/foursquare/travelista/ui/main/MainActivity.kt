package com.foursquare.travelista.ui.main

import android.arch.lifecycle.Observer
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Recommended
import com.foursquare.travelista.databinding.ActivityMainBinding
import com.foursquare.travelista.ui.calendar.CalendarActivity
import com.foursquare.travelista.ui.generate.GenerateActivity
import com.foursquare.travelista.ui.invitefriends.InviteFriendsActivity
import com.foursquare.travelista.ui.login.LoginActivity
import com.foursquare.travelista.ui.mytrip.MyTripActivity
import com.foursquare.travelista.ui.notification.NotificationActivity
import com.foursquare.travelista.ui.profile.ProfileActivity
import com.foursquare.travelista.util.obtainViewModel
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.intentFor


class MainActivity : AppCompatActivity() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var viewDataBinding: ActivityMainBinding
    private var viewAdapter: MainActivityAdapter? = null
    private lateinit var viewManager: RecyclerView.LayoutManager

    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mainViewModel = obtainViewModel()
        viewDataBinding.apply {
            viewmodel = mainViewModel
        }
        subscribeData()
        setList()
        mainViewModel.syncData()
    }

    private fun subscribeData() {
        mainViewModel.apply {

            isCompleted.observe(this@MainActivity, Observer {
                it?.let {
                    if (it) {
                        this.tripList.value?.let {
                            viewAdapter!!.setData(it)
                        }
                    }
                }
            })
        }
    }

    private fun setList() {

        val linearLayoutManager = LinearLayoutManager(this)
        viewDataBinding.recyclerView.layoutManager = linearLayoutManager

        viewAdapter = MainActivityAdapter(arrayListOf(), mainViewModel,
                object : MainActivityAdapter.OnItemClickListener {
                    override fun onItemClick(item: Recommended) {
                        goRecommendedDetail(item)
                    }
                })

        viewDataBinding.recyclerView.adapter = viewAdapter
    }


    fun goRecommendedDetail(recommended: Recommended) {
        startActivity(intentFor<MainTripDetailActivity>("recommended" to recommended))
    }

    override fun onStart() {
        super.onStart()

        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            // Name, email address, and profile photo Url
            val name = user.displayName
            val email = user.email
            val photoUrl = user.photoUrl

            // Check if user's email is verified
            val emailVerified = user.isEmailVerified

            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getToken() instead.
            val uid = user.uid


            //toast("Hello " + user.displayName)
        }

    }

    private fun obtainViewModel(): MainViewModel = obtainViewModel(MainViewModel::class.java)


    fun goToGenerate(view: View) {
        startActivity(Intent(this@MainActivity, GenerateActivity::class.java))
    }

    fun goToFriend(view: View) {
        startActivity(Intent(this@MainActivity, InviteFriendsActivity::class.java))
    }


    fun goToCalendar(view: View) {
        startActivity(Intent(this@MainActivity, CalendarActivity::class.java))
    }


    fun toProfileActivity(view: View) {

        val intent = Intent(this, ProfileActivity::class.java)
        startActivity(intent)

    }

    fun toMyTripActivity(view: View) {

        val intent = Intent(this, MyTripActivity::class.java)
        startActivity(intent)

    }

    fun toNotificationActivity(view: View) {

        val intent = Intent(this, NotificationActivity::class.java)
        startActivity(intent)

    }

    fun singout(view: View) {

        FirebaseAuth.getInstance().signOut()
        LoginManager.getInstance().logOut()

        try {
            if (AccessToken.getCurrentAccessToken() != null) {
                LoginManager.getInstance().logOut()

                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)

            }
        } catch (e1: Exception) {
            e1.printStackTrace()
        }

    }

}


