package com.foursquare.travelista.ui.main

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Recommended
import com.foursquare.travelista.databinding.MainRowBinding


class MainActivityAdapter(var data: ArrayList<Recommended>, var mainViewModel: MainViewModel, var listener: OnItemClickListener) :
RecyclerView.Adapter<MainActivityAdapter.TripViewHolder>() {


    interface OnItemClickListener {
        fun onItemClick(item: Recommended)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding:MainRowBinding = DataBindingUtil.inflate(layoutInflater, R.layout.main_row, parent, false)

        return TripViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size

    fun setData(tripList: List<Recommended>) {
        this.data = ArrayList(tripList)
        //mainViewModel.trip.postValue(tripList)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: TripViewHolder, position: Int) {
        holder.bind(data[position])

    }
// Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.

    inner class TripViewHolder(val binding: MainRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Recommended) {

            binding.apply {
                mainview = mainViewModel
            }
            binding.recommended = data
            binding.executePendingBindings()

            Glide.with(itemView)
                    .load(data.background)
                    .into(binding.imageViewPlace)
            itemView.setOnClickListener {
                listener.onItemClick(data)
            }
        }
    }

}