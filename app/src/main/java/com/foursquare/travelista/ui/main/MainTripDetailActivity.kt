package com.foursquare.travelista.ui.main

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Recommended
import com.foursquare.travelista.databinding.ActivityMainDetailBinding
import com.foursquare.travelista.ui.mytrip.MyTripImageSliderAdapter
import com.foursquare.travelista.ui.mytrip.MyTripPlacesAdapter
import com.foursquare.travelista.util.obtainViewModel
import android.arch.lifecycle.Observer
import android.graphics.Bitmap
import android.view.View

class MainTripDetailActivity : AppCompatActivity()  {

    private lateinit var mainViewModel: MainTripDetailViewModel
    private lateinit var viewDataBinding: ActivityMainDetailBinding
    private var adapter: MyTripPlacesAdapter? = null
    private var sliderAdapter: MyTripImageSliderAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main_detail)
        mainViewModel = obtainViewModel()
        viewDataBinding.apply{
            viewmodel = mainViewModel
        }

        mainViewModel.syncData()
        if (intent.extras != null) {
            val recommended = intent.getParcelableExtra<Recommended>("recommended")
            mainViewModel.trip.postValue(recommended)
            mainViewModel.sync(recommended)
            viewDataBinding.recommended = recommended
        }
        setupList()
        subscribeData(mainViewModel)
        viewDataBinding.imageViewBack.setOnClickListener(View.OnClickListener {
            goMainActivity()
        })
    }

    fun goMainActivity() {
        finish()
    }

    private fun obtainViewModel(): MainTripDetailViewModel = obtainViewModel(MainTripDetailViewModel::class.java)

    private fun subscribeData(mainViewModel: MainTripDetailViewModel) {
        mainViewModel?.let {
            it.isCompleted.observe(this, Observer { it1: Boolean? ->
                it1?.let { it1: Boolean ->
                    if (it1) {
                        adapter!!.setData(it.planList.value, it.placeList.value)
                    }

                }
            })

            it.placeInPlan.observe(this, Observer {
                it?.let { setupImageSlider(it) }
            })
        }
    }

    private fun setupList() {
        val linearLayoutManager = LinearLayoutManager(this)
        viewDataBinding.recycleView1.layoutManager = linearLayoutManager

        adapter = MyTripPlacesAdapter(arrayListOf(), arrayListOf(), object : MyTripPlacesAdapter.OnItemClickListener {
            override fun onItemClick(item: Place) {
            }
        })
        viewDataBinding.recycleView1.adapter = adapter
    }

    private fun setupImageSlider(list: List<Place>) {
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        viewDataBinding.imageViewSlider.layoutManager = linearLayoutManager
        sliderAdapter = MyTripImageSliderAdapter(ArrayList(list), object : MyTripImageSliderAdapter.OnItemClickListener {
            override fun onItemClick(item: Bitmap) {
            }
        })
        viewDataBinding.imageViewSlider.adapter = sliderAdapter
    }
}