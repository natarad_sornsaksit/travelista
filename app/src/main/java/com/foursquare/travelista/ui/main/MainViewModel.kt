/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foursquare.travelista.ui.main

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.foursquare.travelista.SingleLiveEvent
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.data.Recommended
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import java.text.DecimalFormat


class MainViewModel(
        context: Application
) : AndroidViewModel(context) {

    val tripList = MutableLiveData<List<Recommended>>()

    val trip = MutableLiveData<Recommended>()

    val placeList = MutableLiveData<List<Place>>()
    val planList = mutableMapOf<String, MutableList<Plan>>()
    val isCompleted = SingleLiveEvent<Boolean>()

    val placeInPlan = MutableLiveData<List<Place>>()


    fun syncData() {
        Log.i("syncData", "run")
        FirebaseFirestore.getInstance()
                .collection("Recommended")
                .get()
                .addOnCompleteListener {
                    if (it.isComplete) {
                        val trips = mutableListOf<Recommended>()
                        it.result.forEach {
                            val trip = it.toObject(Recommended::class.java)
                            Log.i("Recommended", it.data.toString())
                            trips.add(trip)
                        }

                        if (trips.isNotEmpty()) {
                            tripList.value = trips
                        }

                    } else {
                        Log.e("isComplete ${it.isComplete}", it.exception.toString())
                    }
                }
                .addOnFailureListener {
                    Log.e("syncData", it.toString())
                }.continueWithTask {
                    sync()
                }
    }

    private fun sync(): Task<Void> {
        val listTasks = mutableListOf<Task<QuerySnapshot>>()
        listTasks.add(syncPlace())
        tripList.value?.forEach {
            listTasks.add(findDetail(it))
        }

        return Tasks.whenAll(listTasks)
                .addOnFailureListener {
                    Log.e("sync fail", it.toString())
                }
                .addOnCompleteListener {
                    if (it.isComplete) {
                        tripList.value?.forEach {
                            val plans = planList[it.planId]
                            plans?.let { it1 ->
                                calculateBudget(it1, it.planId)
                            }
                        }

                        isCompleted.postValue(true)
                    }
                }

    }

    private fun calculateBudget(plans: MutableList<Plan>, planId: String) {

        var calBudget: Int = 0
        val placeInPlan: MutableList<Place> = mutableListOf()

        plans.forEach { plan: Plan ->
            val placeFind = placeList.value?.find {
                it.id == plan.place
            }
            placeFind?.let {
                placeInPlan.add(it)
                calBudget += it.budget
            }
        }

        val formatter = DecimalFormat("#,###,###")
        val yourFormattedString = formatter.format(calBudget)

        tripList.value?.find { it.planId == planId }?.let {
            it.budget = "Budget $yourFormattedString Baht"
        }

    }

    fun findDetail(recommended: Recommended): com.google.android.gms.tasks.Task<QuerySnapshot> {
        return FirebaseFirestore.getInstance()
                .collection("Plans")
                .document(recommended.planId)
                .collection("Activity")
                .orderBy("ArrivedTime", Query.Direction.ASCENDING)
                .get()
                .addOnCompleteListener {
                    val plans = mutableListOf<Plan>()
                    if (it.isComplete) {
                        it.result.forEach {
                            val planDetail = it.toObject(Plan::class.java)
                            plans.add(planDetail)
                            Log.i("findDetail", planDetail.toString())
                        }
                        if (plans.isNotEmpty()) {
                            planList[recommended.planId] = plans
                            calculateBudget(plans, recommended.planId)
                        }
                    }
                }.addOnFailureListener {
                    Log.e("findDetail", it.toString())
                }
    }


    fun syncPlace(): com.google.android.gms.tasks.Task<QuerySnapshot> {
        return FirebaseFirestore.getInstance()
                .collection("Places")
                .get()
                .addOnFailureListener {

                }
                .addOnCompleteListener {
                    val places = mutableListOf<Place>()

                    if (it.isComplete) {
                        it.result.forEach {
                            val planDetail = it.toObject(Place::class.java)
                            Log.i("findDetail", planDetail.toString())
                            places.add(planDetail.apply { ->
                                this.id = it.id
                            })
                        }
                    }

                    if (places.isNotEmpty()) {
                        placeList.value = places
                    }
                }
    }


}
