package com.foursquare.travelista.ui.mytrip

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.foursquare.travelista.R
import com.foursquare.travelista.custom.datetimepicker.SlideDateTimeListener
import com.foursquare.travelista.custom.datetimepicker.SlideDateTimePicker
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.data.Trip
import com.foursquare.travelista.databinding.ActivityEditMyTripBinding
import com.foursquare.travelista.util.obtainViewModel
import com.foursquare.travelista.util.showSnackbar
import com.foursquare.travelista.util.toLatLng
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import java.util.*

class EditMyTripActivity : AppCompatActivity() {
    private lateinit var editTripViewModel: EditMyTripViewModel
    private lateinit var viewDataBinding: ActivityEditMyTripBinding
    private lateinit var adapter: EditTripAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_edit_my_trip)
        editTripViewModel = obtainViewModel()
        viewDataBinding.apply {
            viewmodel = editTripViewModel
        }

        if (intent.extras != null) {
            editTripViewModel.trip.value = intent.getParcelableExtra<Trip>("trip")
            editTripViewModel.planList.value = intent.getParcelableArrayListExtra<Plan>("plan")
            editTripViewModel.placeList.value = intent.getParcelableArrayListExtra<Place>("place")
            setupList()
        }

        observeViewModel()

    }

    private fun observeViewModel() {
        editTripViewModel?.let {
            it.planList.observe(this, android.arch.lifecycle.Observer {
                adapter.setPlanAndPlace(editTripViewModel.getPlanAndPlace())
            })

            it.isCompleted.observe(this, android.arch.lifecycle.Observer {
                it?.let {
                    if (it) {
                        goBack()
                    }
                }
            })
        }
    }

    private fun goBack() {
        finish()
    }

    fun goBack(view: View) {
        goBack()
    }

    private fun obtainViewModel(): EditMyTripViewModel =
        obtainViewModel(EditMyTripViewModel::class.java)

    private fun setupList() {
        val linearLayoutManager = LinearLayoutManager(this)
        viewDataBinding.recyclerView.layoutManager = linearLayoutManager

        adapter = EditTripAdapter(
            editTripViewModel.getPlanAndPlace(),
            editTripViewModel.trip.value!!,
            editTripViewModel,
            object : EditTripAdapter.OnItemClickListener {
                override fun onDateClick(plan: Plan, position: Int) {
                    createPlan(plan, position)
                }

                override fun onPlaceClick(place: Place, position: Int) {
                    Dexter.withActivity(this@EditMyTripActivity)
                        .withPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
                        .withListener(object : PermissionListener {
                            override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                                response?.let {
                                    place.Location?.let {
                                        editTripViewModel.lastPosition = position
                                        openPlacePicker(it.toLatLng())
                                    }
                                }
                            }

                            override fun onPermissionRationaleShouldBeShown(
                                permission: PermissionRequest?,
                                token: PermissionToken?
                            ) {
                            }

                            override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                            }
                        }).check()
                }

                override fun onTripStartDateClick() {
                    showDateStart()
                }

                override fun onTripEndDateClick() {
                    showDateEnd()
                }
            })
        viewDataBinding.recyclerView.adapter = adapter
    }


    private fun createPlan(plan: Plan, position: Int) {
        SlideDateTimePicker.Builder(supportFragmentManager)
            .setListener(object : SlideDateTimeListener() {
                override fun onDateTimeSet(date: Date?) {
                    date?.let {
                        Log.i("onDateTimeSet", date.toString())
                        editTripViewModel.isEditPlan = true
                        plan.arrivedTime = it

                        val planList = editTripViewModel.planList.value
                        planList?.let {
                            it[position] = plan
                            it.sortBy { it.arrivedTime }
                            it.forEach {
                                it.day = computeDay(it.arrivedTime)
                            }
                        }

                        editTripViewModel.planList.postValue(planList)

                    }
                }
            })
            .setMinDate(editTripViewModel.trip.value?.startDate)
            .setMaxDate(editTripViewModel.trip.value?.endDate)
            .setInitialDate(plan.arrivedTime)
            .setIs24HourTime(true)
            .build()
            .show()
    }


    private fun computeDay(date: Date): Int {
        val startDate =
            LocalDateTime.ofInstant(
                Instant.ofEpochMilli(editTripViewModel.trip.value!!.startDate!!.time),
                ZoneId.systemDefault()
            )

        val targetDate = LocalDateTime.ofInstant(
            Instant.ofEpochMilli(date.time),
            ZoneId.systemDefault()
        )

        return Duration.between(startDate, targetDate).toDays().toInt() + 1
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                editTripViewModel.isEditPlace = true
                val place = PlacePicker.getPlace(this, data)
                editTripViewModel.addPlace(place, Date())
            }
        }
    }

    private fun openPlacePicker(latLng: LatLng) {
        val builder = PlacePicker.IntentBuilder()
        val rangN = LatLng(latLng.latitude + 0.005, latLng.longitude + 0.005)
        val rangS = LatLng(latLng.latitude - 0.005, latLng.longitude - 0.005)

        //set start point
        val bounds: LatLngBounds = LatLngBounds.Builder()
            .include(rangN)
            .include(rangS).build()

        builder.setLatLngBounds(bounds)
        startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }


    fun showDateEnd() {
        val myCalendar = Calendar.getInstance()
        val startDate = editTripViewModel.trip.value!!.startDate

        val date = DatePickerDialog.OnDateSetListener { dialog, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)


            if (startDate.time > myCalendar.time.time) {
                dialog.showSnackbar(
                    "Please select end date more than start date",
                    Snackbar.LENGTH_SHORT
                )
            } else {
                editTripViewModel.trip.value!!.endDate = myCalendar.time
                adapter.updateTrip(editTripViewModel.trip.value!!)

                editTripViewModel.isEditTrip = true
            }
        }
        val dp = DatePickerDialog(
            this@EditMyTripActivity,
            date,
            myCalendar.get(Calendar.YEAR),
            myCalendar.get(Calendar.MONTH),
            myCalendar.get(Calendar.DAY_OF_MONTH)
        )
        if (startDate.time > 0) {
            dp.datePicker.minDate = startDate.time
        }

        editTripViewModel.trip.value?.let {
            val endDate = Calendar.getInstance()
            endDate.time = it.endDate
            dp.updateDate(
                endDate.get(Calendar.YEAR),
                endDate.get(Calendar.MONTH),
                endDate.get(Calendar.DAY_OF_MONTH)
            )
        }

        dp.show()
    }

    fun showDateStart() {
        val myCalendar = Calendar.getInstance()
        val endDate = editTripViewModel.trip.value!!.endDate

        val date = DatePickerDialog.OnDateSetListener { dialog, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            if (endDate.time < myCalendar.time.time) {
                dialog.showSnackbar(
                    "Please select start date less than end date",
                    Snackbar.LENGTH_SHORT
                )
            } else {
                editTripViewModel.trip.value!!.startDate = myCalendar.time
                adapter.updateTrip(editTripViewModel.trip.value!!)
                editTripViewModel.isEditTrip = true
            }
        }
        val dp = DatePickerDialog(
            this@EditMyTripActivity, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
            myCalendar.get(Calendar.DAY_OF_MONTH)
        )
        dp.datePicker.maxDate = endDate.time

        editTripViewModel.trip.value?.let {
            val currentDate = Calendar.getInstance()
            currentDate.time = it.startDate
            dp.updateDate(
                currentDate.get(Calendar.YEAR),
                currentDate.get(Calendar.MONTH),
                currentDate.get(Calendar.DAY_OF_MONTH)
            )
        }
        dp.show()
    }

    companion object {
        private const val PLACE_PICKER_REQUEST = 1
    }

}
