package com.foursquare.travelista.ui.mytrip

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableBoolean
import android.util.Log
import com.foursquare.travelista.SingleLiveEvent
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.data.Trip
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import java.util.*

class EditMyTripViewModel(
    context: Application
) : AndroidViewModel(context) {
    val trip = MutableLiveData<Trip>()
    val placeList = MutableLiveData<MutableList<Place>>()
    val planList = MutableLiveData<MutableList<Plan>>()

    val isLoading = ObservableBoolean(false)
    val isUpdating = ObservableBoolean(false)
    val isCompleted = SingleLiveEvent<Boolean>()

    var isEditPlan: Boolean = false
    var isEditPlace: Boolean = false
    var isEditTrip: Boolean = false

    var lastPosition = 0

    fun getPlanAndPlace(): ArrayList<Pair<Plan, Place>> {
        var lastDay = 0
        val pairList = arrayListOf<Pair<Plan, Place>>()
        planList.value?.forEach { plan ->
            val place = placeList.value!!.find { plan.place == it.id }
            plan.isShowHeader = lastDay < plan.day
            lastDay = plan.day
            place?.let {
                pairList.add(Pair(plan, it))
            }
        }
        return pairList
    }


    fun addPlace(place: com.google.android.gms.location.places.Place, date: Date) {
        isLoading.set(true)
        val newPlace = Place(
                Location = GeoPoint(place.latLng.latitude, place.latLng.longitude),
                nameEN = place.name.toString(),
                nameTH = place.name.toString(),
                provinceEN = place.address.toString(),
                provinceTH = place.address.toString()
        ).apply {
            id = place.id
            this.date = date
        }

        addPlaceToFirestore(newPlace)
        setPlaceToPlan(newPlace)

        placeList.postValue(
            placeList.value.apply {
                this!!.add(
                    newPlace
                )
            }
        )
    }

    fun setEditTrip(text: CharSequence) {
        Log.i("setEditTrip", "$isEditTrip")
        trip.value?.let {
            it.name = text.toString()
        }
        isEditTrip = true
    }

    fun setEditTrip() {
        Log.i("setEditTrip", "$isEditTrip")
        isEditTrip = true
    }

    private fun setPlaceToPlan(place: Place) {
        planList.value?.let {
            it.set(lastPosition,
                it[lastPosition].apply {
                    this.place = place.id
                }
            )
        }
        isEditPlan = true
        planList.postValue(planList.value)
    }

    private fun addPlaceToFirestore(place: Place) {
        FirebaseFirestore.getInstance()
            .collection("Places")
            .document(place.id)
            .set(place.toMap())
            .addOnCompleteListener {
                isLoading.set(false)
            }
            .addOnFailureListener {
                Log.e("addPlaceToFirestore", it.toString())
                isLoading.set(false)
            }
    }


    fun updateTrip() {
        if (isEditPlan or isEditTrip) {
            isUpdating.set(true)
            val tasks: MutableCollection<Task<Void>> = mutableListOf()
            if (isEditPlan) {
                tasks.addAll(getPlanTask())
            }

            tasks.add(
                FirebaseFirestore
                    .getInstance()
                    .collection("Trips")
                    .document(trip.value?.id!!)
                    .set(trip.value!!.toMap())
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            Log.i("isTripEdit", "${it.isComplete}")

                        } else {
                            Log.e("isTripEdit", it.exception.toString())
                        }
                    }
                    .addOnFailureListener {
                        Log.e("isTripEdit", it.toString())
                    }
            )


            Tasks.whenAll(tasks)
                .addOnCompleteListener {
                    isUpdating.set(false)
                    isCompleted.postValue(true)
                }
                .addOnFailureListener {
                    isUpdating.set(false)
                    isCompleted.postValue(false)
                    Log.e("updateTrip", it.toString())
                }
        } else {

        }
    }

    private fun getPlanTask(): MutableCollection<Task<Void>> {
        val planTasks: MutableCollection<Task<Void>> = mutableListOf()
        planList.value?.let {
            it.forEach {
                planTasks.add(FirebaseFirestore.getInstance()
                    .collection("Plans")
                    .document(trip.value!!.planId)
                    .collection("Activity")
                    .document(it.id)
                    .set(it.toMap())
                    .addOnFailureListener {
                        Log.e("getPlanTask", it.toString())
                    }
                    .addOnCompleteListener {

                    }
                )
            }
        }
        return planTasks
    }
}