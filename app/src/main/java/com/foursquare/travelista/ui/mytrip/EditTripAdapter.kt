package com.foursquare.travelista.ui.mytrip

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.data.Trip
import com.foursquare.travelista.databinding.ItemEditTripBinding
import com.foursquare.travelista.databinding.ItemEditTripHeaderBinding

class EditTripAdapter(
        internal var data: List<Pair<Plan, Place>>,
        var trip: Trip,
        val viewmodel: EditMyTripViewModel,
        var listener: OnItemClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface OnItemClickListener {
        fun onDateClick(plan: Plan, position: Int)
        fun onPlaceClick(place: Place, position: Int)
        fun onTripStartDateClick()
        fun onTripEndDateClick()

    }

    fun setPlanAndPlace(list: List<Pair<Plan, Place>>) {
        this.data = list
        notifyDataSetChanged()
    }

    fun updateTrip(trip: Trip) {
        this.trip = trip
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_ITEM) {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding: ItemEditTripBinding =
                DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.item_edit_trip,
                    parent,
                    false
                )
            return VHItem(binding)
        } else if (viewType == TYPE_HEADER) {
            //inflate your layout and pass it to view holder
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding: ItemEditTripHeaderBinding =
                DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.item_edit_trip_header,
                    parent,
                    false
                )

            return VHHeader(binding)
        }

        throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VHItem) {
            val dataItem = getItem(position)
            //cast holder to VHItem and set data
            holder.bindData(dataItem)
            holder.binding.etPlanDate.setOnKeyListener(null)
        } else if (holder is VHHeader) {
            //cast holder to VHHeader and set data for header.
            holder.bindData(trip, viewmodel)
            holder.binding.etStartDate.setOnKeyListener(null)
            holder.binding.etEndDate.setOnKeyListener(null)

            holder.setupSpinner()

        }
    }

    override fun getItemCount(): Int {
        return data.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        if (trip != null) {
            return if (position == 0) {
                TYPE_HEADER
            } else {
                TYPE_ITEM
            }

        } else {
            return TYPE_ITEM
        }

    }

    private fun getItem(position: Int): Pair<Plan, Place> {
        return data[position - 1]
    }

    inner class VHItem(val binding: ItemEditTripBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindData(pair: Pair<Plan, Place>) {
            binding.plan = pair.first
            binding.place = pair.second
            binding.etPlaceName.setOnClickListener {
                listener.onPlaceClick(pair.second, data.indexOf(pair))
            }
            binding.etPlanDate.setOnClickListener {
                listener.onDateClick(pair.first, data.indexOf(pair))
            }

        }
    }

    inner class VHHeader(val binding: ItemEditTripHeaderBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindData(trip: Trip, viewmodel: EditMyTripViewModel) {
            binding.trip = trip
            binding.viewmodel = viewmodel
            binding.notifyChange()
            binding.etEndDate.setOnClickListener {
                listener.onTripEndDateClick()
            }
            binding.etStartDate.setOnClickListener {
                listener.onTripStartDateClick()
            }
        }

        fun setupSpinner() {
            val provinces = arrayListOf(
                "กระบี่",
                "กรุงเทพมหานคร",
                "กาญจนบุรี",
                "กาฬสินธุ์",
                "กำแพงเพชร",
                "ขอนแก่น",
                "จันทบุรี",
                "ฉะเชิงเทรา",
                "ชลบุรี",
                "ชัยนาท",
                "ชัยภูมิ",
                "ชุมพร",
                "เชียงราย",
                "เชียงใหม่",
                "ตรัง",
                "ตราด",
                "ตาก",
                "นครนายก",
                "นครปฐม",
                "นครพนม",
                "นครราชสีมา",
                "นครศรีธรรมราช",
                "นครสวรรค์",
                "นนทบุรี",
                "นราธิวาส",
                "น่าน",
                "บุรีรัมย์",
                "ปทุมธานี",
                "ประจวบคีรีขันธ์",
                "ปราจีนบุรี",
                "ปัตตานี",
                "พะเยา",
                "พังงา",
                "พัทลุง",
                "พิจิตร",
                "พิษณุโลก",
                "เพชรบุรี",
                "เพชรบูรณ์",
                "แพร่",
                "ภูเก็ต",
                "มหาสารคาม",
                "มุกดาหาร",
                "แม่ฮ่องสอน",
                "ยโสธร",
                "ยะลา",
                "ร้อยเอ็ด",
                "ระนอง",
                "ระยอง",
                "ราชบุรี",
                "ลพบุรี",
                "ลำปาง",
                "ลำพูน",
                "เลย",
                "ศรีสะเกษ",
                "สกลนคร",
                "สงขลา",
                "สตูล",
                "สมุทรปราการ",
                "สมุทรสงคราม",
                "สมุทรสาคร",
                "สระแก้ว",
                "สระบุรี",
                "สิงห์บุรี",
                "สุโขทัย",
                "สุพรรณบุรี",
                "สุราษฎร์ธานี",
                "สุรินทร์",
                "หนองคาย",
                "หนองบัวลำภู",
                "อยุธยา",
                "อ่างทอง",
                "อำนาจเจริญ",
                "อุดรธานี",
                "อุตรดิตถ์",
                "อุทัยธานี",
                "อุบลราชธานี"
            )
            val adapter = ArrayAdapter(
                binding.root.context,
                android.R.layout.simple_dropdown_item_1line, provinces
            )
            binding.etLocation.adapter = adapter
            binding.etLocation.onItemSelectedListener =
                    object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(
                            parent: AdapterView<*>,
                            view: View,
                            position: Int,
                            id: Long
                        ) {
                            val result = parent.getItemAtPosition(position) as String
                            binding.trip!!.province = result
                            viewmodel.setEditTrip()
                        }

                        override fun onNothingSelected(parent: AdapterView<*>) {

                        }
                    }
            provinces.indexOf(binding.trip!!.province)?.let {
                binding.etLocation.setSelection(it)
            }

        }


    }

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }
}