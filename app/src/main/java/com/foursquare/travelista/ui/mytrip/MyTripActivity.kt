package com.foursquare.travelista.ui.mytrip

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Trip
import com.foursquare.travelista.databinding.ActivityMyTripBinding
import com.foursquare.travelista.util.obtainViewModel
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.intentFor

class MyTripActivity: AppCompatActivity() {

    private lateinit var myTripViewModel: MyTripViewModel
    private lateinit var viewDataBinding: ActivityMyTripBinding
    private lateinit var recyclerView: RecyclerView
    private var viewAdapter: MyTripActivityAdapter? = null
    private lateinit var viewManager: RecyclerView.LayoutManager

    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_trip)
        myTripViewModel = obtainViewModel()
        viewDataBinding.apply {
            viewmodel = myTripViewModel
        }

        subscribeData()
        setList()
    }

    override fun onResume() {
        super.onResume()
        myTripViewModel.syncData()
    }

    private fun subscribeData() {
        myTripViewModel.apply {
            tripList.observe(this@MyTripActivity, android.arch.lifecycle.Observer {
                it?.let {
                    it?.let {
                        viewAdapter!!.setData(it)
                    }
                }

            })
        }
    }

    private fun setList() {

        viewManager = LinearLayoutManager(this)
        viewAdapter = MyTripActivityAdapter(arrayListOf(), object : MyTripActivityAdapter.OnItemClickListener {
            override fun onItemClick(item: Trip) {
                goToMyTripDetail(item)
            }
        })

        recyclerView = findViewById<RecyclerView>(R.id.recyclerViewMyTrip).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }
    }

    fun goToMyTripDetail(trip: Trip) {
        startActivity(intentFor<MyTripDetailActivity>("trip" to trip))
    }

    fun end(view: View) {

        finish()

    }

    private fun obtainViewModel(): MyTripViewModel = obtainViewModel(MyTripViewModel::class.java)
}