package com.foursquare.travelista.ui.mytrip

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Trip
import com.foursquare.travelista.databinding.MytripRowBinding

class MyTripActivityAdapter(var data: ArrayList<Trip>, var listener: OnItemClickListener) :
    RecyclerView.Adapter<MyTripActivityAdapter.TripViewHolder>() {


    interface OnItemClickListener {
        fun onItemClick(item: Trip)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: MytripRowBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.mytrip_row, parent, false)

        return TripViewHolder(binding);
    }

    override fun getItemCount(): Int = data.size

    fun setData(tripList: List<Trip>) {
        this.data = ArrayList(tripList)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: TripViewHolder, position: Int) {
        holder.bind(data[position])
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.

    inner class TripViewHolder(val binding: MytripRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Trip) {
            binding.mytrip = data
            binding.executePendingBindings()
            itemView.setOnClickListener {
                listener.onItemClick(data)
            }

//            Glide.with(itemView)
//                    .load(data.background)
//                    .into(binding.imageViewPlace)
////            itemView.setOnClickListener {
//                listener.onItemClick(data)
//            }
        }
    }

}