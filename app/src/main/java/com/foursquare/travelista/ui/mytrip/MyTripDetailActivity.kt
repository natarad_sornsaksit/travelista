package com.foursquare.travelista.ui.mytrip

import android.arch.lifecycle.Observer
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Trip
import com.foursquare.travelista.databinding.ActivityMyTripDetailBinding
import com.foursquare.travelista.ui.calendar.CalendarGroupActivity
import com.foursquare.travelista.ui.invitefriends.InviteFriendsActivity
import com.foursquare.travelista.util.obtainViewModel
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.intentFor

class MyTripDetailActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null

    private lateinit var myTripViewModel: MyTripViewModel
    private lateinit var viewDataBinding: ActivityMyTripDetailBinding
    private var adapter: MyTripPlacesAdapter? = null
    private var sliderAdapter: MyTripImageSliderAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_trip_detail)
        myTripViewModel = obtainViewModel()
        viewDataBinding.apply {
            viewmodel = myTripViewModel
        }

        myTripViewModel.syncData()
        if (intent.extras != null) {
            val trip = intent.getParcelableExtra<Trip>("trip")
            myTripViewModel.trip.postValue(trip)
            myTripViewModel.sync(trip)
            viewDataBinding.trip = trip
        }
        setupList()
        subscribeData(myTripViewModel)

        Glide.with(this)
            .load(mAuth!!.currentUser?.photoUrl.toString())
            .apply(RequestOptions.circleCropTransform())
            .into(viewDataBinding.imageView32)

        viewDataBinding.imageViewEdit.setOnClickListener { goToEditTrip() }
        viewDataBinding.ivAdd.setOnClickListener { ontoInvite() }
        viewDataBinding.imageViewBack.setOnClickListener {
            goTripActivity()
        }

    }

    fun goTripActivity() {
        finish()
    }

    override fun onResume() {
        super.onResume()
        myTripViewModel.trip.value?.let {
            myTripViewModel.syncTripDetail(it)
            myTripViewModel.sync(it)
        }
    }


    private fun ontoInvite() {

        startActivity(Intent(this@MyTripDetailActivity, InviteFriendsActivity::class.java))
    }

    private fun ontoCalendar() {
        startActivity(Intent(this@MyTripDetailActivity, CalendarGroupActivity::class.java))
    }

    private fun goToEditTrip() {
        startActivity(
            intentFor<EditMyTripActivity>
                (
                "trip" to myTripViewModel.trip.value,
                "plan" to myTripViewModel.planList.value,
                "place" to myTripViewModel.placeInPlan.value
            )
        )
    }

    private fun obtainViewModel(): MyTripViewModel = obtainViewModel(MyTripViewModel::class.java)

    private fun subscribeData(myTripViewModel: MyTripViewModel) {
        myTripViewModel?.let {
            it.isCompleted.observe(this, Observer { it1: Boolean? ->
                it1?.let { it1: Boolean ->
                    if (it1) {
                        adapter!!.setData(it.planList.value, it.placeList.value)
                    }

                }
            })

            it.placeInPlan.observe(this, Observer {
                it?.let { setupImageSlider(it) }
            })

            it.trip.observe(this, Observer {
                viewDataBinding.trip = it
            })
        }
    }

    private fun setupList() {
        val linearLayoutManager = LinearLayoutManager(this)
        viewDataBinding.recycleView1.layoutManager = linearLayoutManager

        adapter = MyTripPlacesAdapter(
            arrayListOf(),
            arrayListOf(),
            object : MyTripPlacesAdapter.OnItemClickListener {
                override fun onItemClick(item: Place) {
                    openMap(item)
                }
            })
        viewDataBinding.recycleView1.adapter = adapter
    }

    private fun openMap(place: Place) {
        val fullAddress = place.provinceEN
        val location = place.Location!!
        val gmmIntentUri =
            Uri.parse("geo:0,0?q=${location.latitude},${location.longitude}($fullAddress)")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        startActivity(mapIntent)
    }

    private fun setupImageSlider(list: List<Place>) {
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        viewDataBinding.imageViewSlider.layoutManager = linearLayoutManager
        sliderAdapter = MyTripImageSliderAdapter(
            ArrayList(list),
            object : MyTripImageSliderAdapter.OnItemClickListener {
                override fun onItemClick(item: Bitmap) {
                    Glide.with(this@MyTripDetailActivity)
                        .load(item)
                        .apply(RequestOptions().centerCrop())
                        .into(viewDataBinding.ivBackground)
                }
            })
        viewDataBinding.imageViewSlider.adapter = sliderAdapter
    }
}