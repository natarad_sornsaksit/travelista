package com.foursquare.travelista.ui.mytrip

import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.databinding.ItemMytripImageSliderBinding
import com.google.android.gms.location.places.Places

class MyTripImageSliderAdapter(
        var place: ArrayList<Place>,
        var listener: MyTripImageSliderAdapter.OnItemClickListener
) : RecyclerView.Adapter<MyTripImageSliderAdapter.TripViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyTripImageSliderAdapter.TripViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemMytripImageSliderBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_mytrip_image_slider,
                parent,
                false
            )

        return TripViewHolder(binding)
    }

    override fun getItemCount(): Int = place.size

    override fun onBindViewHolder(holder: TripViewHolder, position: Int) {
        holder.bind(place[position])
    }

    fun setData(tripList: List<Plan>?, value: List<Place>?) {
        this.place = ArrayList(value)
        notifyDataSetChanged()
    }


    interface OnItemClickListener {
        fun onItemClick(item: Bitmap)
    }

    inner class TripViewHolder(val binding: ItemMytripImageSliderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(place: Place) {
            binding.executePendingBindings()

            val geoDataClient = Places.getGeoDataClient(itemView.context)
            geoDataClient.getPlacePhotos(place.id)
                .addOnCompleteListener {
                    if (it.isComplete && it.result.photoMetadata != null && it.result.photoMetadata.count > 0) {
                        geoDataClient.getPhoto(it.result.photoMetadata[0]).addOnCompleteListener {

                            val image = it.result.bitmap
                            Glide.with(itemView.context.applicationContext)
                                .load(image)
                                .apply(
                                    RequestOptions()
                                        .centerCrop()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .placeholder(R.drawable.placeholder)
                                )
                                .into(binding.ivThumbnail)

                            itemView.setOnClickListener {
                                listener.onItemClick(image)
                            }
                        }
                    }
                }

            itemView.setOnClickListener {

            }
        }
    }
}