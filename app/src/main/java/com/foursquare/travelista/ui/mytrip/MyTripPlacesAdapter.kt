package com.foursquare.travelista.ui.mytrip

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.databinding.ItemMyTripPlaceBinding

class MyTripPlacesAdapter(var plan: ArrayList<Plan>, var place: ArrayList<Place>, var listener: OnItemClickListener) : RecyclerView.Adapter<MyTripPlacesAdapter.TripViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: Place)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemMyTripPlaceBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_my_trip_place, parent, false)

        return TripViewHolder(binding)
    }

    override fun getItemCount(): Int = plan.size

    override fun onBindViewHolder(holder: TripViewHolder, position: Int) {
        holder.bind(plan[position], place.find { it.id == plan[position].place })
    }

    fun setData(tripList: List<Plan>?, value: List<Place>?) {
        var lastDay = 0
        tripList?.forEach {
            if (lastDay < it.day) {
                it.isShowHeader = true
                lastDay = it.day
            }
        }
        this.plan = ArrayList(tripList)
        this.place = ArrayList(value)
        notifyDataSetChanged()
    }

    inner class TripViewHolder(val binding: ItemMyTripPlaceBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(plan: Plan, place: Place?) {
            binding.plan = plan
            binding.place = place
            binding.executePendingBindings()
            itemView.setOnClickListener {
                place?.let { it1 -> listener.onItemClick(it1) }
            }
        }
    }

}