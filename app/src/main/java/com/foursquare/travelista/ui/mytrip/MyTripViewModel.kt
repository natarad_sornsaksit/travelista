package com.foursquare.travelista.ui.mytrip

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import android.util.Log
import com.foursquare.travelista.SingleLiveEvent
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.data.Trip
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import java.text.DecimalFormat

class MyTripViewModel(
    context: Application
) : AndroidViewModel(context) {

    val tripList = MutableLiveData<List<Trip>>()

    val trip = MutableLiveData<Trip>()
    val placeList = MutableLiveData<List<Place>>()
    val planList = MutableLiveData<List<Plan>>()
    val isCompleted = SingleLiveEvent<Boolean>()

    val placeInPlan = MutableLiveData<List<Place>>()

    val budget = ObservableField<String>("")

    fun syncData() {
        Log.i("syncData", "run")
        FirebaseAuth.getInstance().currentUser?.let { user ->

            FirebaseFirestore.getInstance()
                .collection("Trips")
                .whereEqualTo("Member.${user.uid}", true)
                .get()
                .addOnCompleteListener {
                    if (it.isComplete) {
                        val trips = mutableListOf<Trip>()
                        it.result.forEach {
                            val trip = it.toObject(Trip::class.java).apply {
                                id = it.id
                            }
                            Log.i("My Trip", it.data.toString())
                            trips.add(trip)
                        }

                        if (trips.isNotEmpty()) {
                            tripList.postValue(trips)
                        }

                    } else {
                        Log.e("isComplete ${it.isComplete}", it.exception.toString())
                    }
                }
                .addOnFailureListener {
                    Log.e("syncData", it.toString())
                }
        }
    }

    fun syncTripDetail(trip: Trip) {
        Log.i("syncTripDetail", "run")
        FirebaseFirestore.getInstance()
            .collection("Trips")
            .document(trip.id)
            .get()
            .addOnCompleteListener {
                if (it.isComplete) {
                    val newTrip = it.result.toObject(Trip::class.java).apply {
                        this!!.id = it.result.id
                    }
                    newTrip?.let {
                        this.trip.postValue(it)

                    }
                } else {
                    Log.e("isComplete ${it.isComplete}", it.exception.toString())
                }
            }
            .addOnFailureListener {
                Log.e("syncData", it.toString())
            }
    }

    fun sync(trip: Trip) {
        Tasks.whenAll(findDetail(trip), syncPlace())
            .addOnFailureListener {

            }
            .addOnCompleteListener {
                if (it.isComplete) {
                    isCompleted.postValue(true)
                    calculateBudget()
                }
            }

    }

    private fun calculateBudget() {
        var calBudget: Int = 0
        val placeInPlan: MutableList<Place> = mutableListOf()
        planList.value?.forEach { plan: Plan ->
            val placeFind = placeList.value?.find {
                it.id == plan.place
            }
            placeFind?.let {
                placeInPlan.add(it)
                calBudget += it.budget
            }
        }

        this.placeInPlan.postValue(placeInPlan)

        val formatter = DecimalFormat("#,###,###")
        val yourFormattedString = formatter.format(calBudget)
        budget.set("Budget $yourFormattedString Baht")
    }

    fun findDetail(trip: Trip): com.google.android.gms.tasks.Task<QuerySnapshot> {
        return FirebaseFirestore.getInstance()
            .collection("Plans")
            .document(trip.planId)
            .collection("Activity")
            .orderBy("ArrivedTime", Query.Direction.ASCENDING)
            .get()
            .addOnCompleteListener {
                val plans = mutableListOf<Plan>()
                if (it.isComplete) {
                    it.result.forEach {
                        val planDetail = it.toObject(Plan::class.java).apply {
                            this.id = it.id
                        }
                        plans.add(planDetail)
                        Log.i("findDetail", planDetail.toString())
                    }
                    if (plans.isNotEmpty()) {

                        planList.value = plans
                    }
                }
            }.addOnFailureListener {
                Log.e("findDetail", it.toString())
            }
    }


    fun syncPlace(): com.google.android.gms.tasks.Task<QuerySnapshot> {
        return FirebaseFirestore.getInstance()
            .collection("Places")
            .get()
            .addOnFailureListener {

            }
            .addOnCompleteListener {
                val places = mutableListOf<Place>()

                if (it.isComplete) {
                    it.result.forEach {
                        val planDetail = it.toObject(Place::class.java)
                        Log.i("findDetail", planDetail.toString())
                        places.add(planDetail.apply { ->
                            this.id = it.id
                        })
                    }
                }

                if (places.isNotEmpty()) {
                    placeList.value = places
                }
            }
    }

}