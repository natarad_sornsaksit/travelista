package com.foursquare.travelista.ui.notification

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Notification
import com.foursquare.travelista.databinding.ActivityNotificationBinding
import com.foursquare.travelista.util.obtainViewModel

class NotificationActivity : AppCompatActivity() {

    private lateinit var notificationViewModel: NotificationViewModel
    private lateinit var viewDataBinding: ActivityNotificationBinding
    private var viewAdapter: notificationViewAdapter? = null

    //private var viewAdapter: NotificationAdapter? = null
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this,R.layout.activity_notification)

        notificationViewModel = obtainViewModel()
        viewDataBinding.apply {
            viewmodel = notificationViewModel }

        subscribeData()
        setList()
        notificationViewModel.syncData()

    }

    private fun subscribeData() {
        notificationViewModel.apply {
            notiList.observe(this@NotificationActivity, android.arch.lifecycle.Observer {
                it?.let {
                    it?.let {
                        viewAdapter!!.setData(it)
                    }
                }

            })
        }
    }

    private fun setList() {
        viewManager = LinearLayoutManager(this)
        viewAdapter = notificationViewAdapter(this,arrayListOf(), object : notificationViewAdapter.OnItemClickListener {
            override fun onItemClick(item: Notification) {

            }
        })

        recyclerView = findViewById<RecyclerView>(R.id.rvNotification).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }
    }

    fun end(view : View?) {

        finish()

    }

    private fun obtainViewModel(): NotificationViewModel = obtainViewModel(NotificationViewModel::class.java)

}