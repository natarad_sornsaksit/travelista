/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foursquare.travelista.ui.notification

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.foursquare.travelista.data.Notification
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore


class NotificationViewModel(
        context: Application
) : AndroidViewModel(context) {

    val notiList = MutableLiveData<List<Notification>>()

    val notifi = MutableLiveData<Notification>()

    var mAuth = FirebaseAuth.getInstance()

    var uid = mAuth.currentUser?.uid


    fun syncData() {
        Log.i("syncData", "run")
        FirebaseFirestore.getInstance()
                .collection("Users/"+uid+"/notification")
                .get()
                .addOnCompleteListener {
                    if (it.isComplete) {
                        val notifications = mutableListOf<Notification>()
                        it.result.forEach {
                            val notifi = it.toObject(Notification::class.java)
                            Log.i("Notification", it.data.toString())
                            notifications.add(notifi)
                        }

                        if (notifications.isNotEmpty()) {
                            notiList.postValue(notifications)
                        }

                    } else {
                        Log.e("isComplete ${it.isComplete}", it.exception.toString())
                    }
                }
                .addOnFailureListener {
                    Log.e("syncData", it.toString())
                }
    }


}




