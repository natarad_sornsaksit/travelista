package com.foursquare.travelista.ui.notification

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Notification
import com.foursquare.travelista.data.Trip
import com.foursquare.travelista.databinding.ItemNotificationBinding
import com.foursquare.travelista.ui.invitetrip.InviteTripActivity
import org.jetbrains.anko.intentFor


class notificationViewAdapter(context: Context, var data: ArrayList<Notification>, var listener: OnItemClickListener) :
        RecyclerView.Adapter<notificationViewAdapter.InviteViewHolder>() {

    private val context: Context


    init {

        this.context = context
    }

    interface OnItemClickListener {
        fun onItemClick(item: Notification)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InviteViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemNotificationBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_notification, parent, false)

        return InviteViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size

    fun setData(notiList: List<Notification>) {
        this.data = ArrayList(notiList)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: InviteViewHolder, position: Int) {
        holder.bind(data[position])
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.

    inner class InviteViewHolder(val binding: ItemNotificationBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Notification) {
            binding.notifications = data
            binding.executePendingBindings()

            binding.tvNotiHead.text = data.displayName
            binding.tvNotiDetail.text = data.topic
            binding.tvTime.text = data.timeStamp

            Glide.with(itemView)
                    .load(data.pictureUrl)
                    .apply(RequestOptions.circleCropTransform())
                    .into(binding.ivNotiProfile)
            itemView.setOnClickListener {


                val trip = Trip()
                trip.planId = data.tripID

                context.startActivity(context.intentFor<InviteTripActivity>("trip" to trip))
                // context.startActivity(Intent(context, RecommendTripDetailActivity::class.java))

            }

            // binding.tvUsername.text = data.displayName
        }
    }

}