package com.foursquare.travelista.ui.profile

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.foursquare.travelista.R
import com.foursquare.travelista.databinding.ActivityProfileBinding
import com.foursquare.travelista.ui.login.LoginActivity
import com.foursquare.travelista.util.obtainViewModel
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth

class ProfileActivity : AppCompatActivity() {

    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var viewDataBinding: ActivityProfileBinding

    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        profileViewModel = obtainViewModel()
        viewDataBinding.apply { viewmodel = profileViewModel }


        initInstance()


    }

    private fun initInstance() {

        Glide.with(this)
                .load(mAuth!!.currentUser?.photoUrl.toString())
                .apply(RequestOptions.circleCropTransform())
                .into(viewDataBinding.ivImgProfile)

        viewDataBinding.tvName.text = mAuth!!.currentUser?.displayName.toString()

    }

    private fun obtainViewModel(): ProfileViewModel = obtainViewModel(ProfileViewModel::class.java)


    fun end(view: View?) {

        finish()

    }

    fun sendTextIntent(view: View?) {

        val ssendIntent = Intent()
        ssendIntent.action = Intent.ACTION_SEND
        ssendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out this app at: https://play.google.com/store/apps/details?id=$packageName")
        ssendIntent.type = "text/plain"
        startActivity(ssendIntent)

    }


    fun singout(view: View?) {

        FirebaseAuth.getInstance().signOut()
        LoginManager.getInstance().logOut()

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)


        try {
            if (AccessToken.getCurrentAccessToken() != null) {
                LoginManager.getInstance().logOut()

                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)

            }
        } catch (e1: Exception) {
            e1.printStackTrace()
        }

    }
}
