package com.foursquare.travelista.ui.recommendtrip

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.databinding.ItemImageSliderBinding

class ImageSliderAdapter(var place: ArrayList<Place>, var listener: OnItemClickListener) : RecyclerView.Adapter<ImageSliderAdapter.WeatherViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: Place)
    }


    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.bind(place[position])
    }

    fun setData(tripList: List<Plan>?, value: List<Place>?) {
        this.place = ArrayList(value)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemImageSliderBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_image_slider, parent, false)

        return WeatherViewHolder(binding)
    }

    override fun getItemCount(): Int = place.size


    inner class WeatherViewHolder(val binding: ItemImageSliderBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(place: Place) {
            binding.executePendingBindings()
            Glide.with(itemView)
                    .load(place.image)
                    .apply(RequestOptions().centerCrop())
                    .into(binding.ivThumbnail)
            itemView.setOnClickListener {
                place?.let { it1 -> listener.onItemClick(it1) }
            }
        }
    }


}

