package com.foursquare.travelista.ui.recommendtrip

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.databinding.ItemPlaceBinding

class PlacesAdapter(var plan: ArrayList<Plan>, var place: ArrayList<Place>, var listener: OnItemClickListener) : RecyclerView.Adapter<PlacesAdapter.WeatherViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: Place)
    }


    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.bind(plan[position], place.find { it.id == plan[position].place })
    }

    fun setData(tripList: List<Plan>?, value: List<Place>?) {
        this.plan = ArrayList(tripList)
        this.place = ArrayList(value)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemPlaceBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_place, parent, false)

        return WeatherViewHolder(binding)
    }

    override fun getItemCount(): Int = plan.size


    inner class WeatherViewHolder(val binding: ItemPlaceBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(plan: Plan, place: Place?) {
            binding.plan = plan
            binding.place = place
            binding.isShowHeader = adapterPosition == 0
            binding.executePendingBindings()
            itemView.setOnClickListener {
                place?.let { it1 -> listener.onItemClick(it1) }
            }
        }
    }


}

