package com.foursquare.travelista.ui.recommendtrip

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Trip
import com.foursquare.travelista.databinding.ActivityRecommendTripBinding
import com.foursquare.travelista.util.obtainViewModel
import org.jetbrains.anko.intentFor

class RecommendTripActivity : AppCompatActivity() {

    private lateinit var recommendViewModel: RecommendViewModel
    private lateinit var viewDataBinding: ActivityRecommendTripBinding
    private var adapter: TripAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_recommend_trip)
        recommendViewModel = obtainViewModel()
        viewDataBinding.apply {
            viewmodel = recommendViewModel
        }

        setSupportActionBar(viewDataBinding.toolbar)
        subscribeData()
        setList()
        recommendViewModel.syncData()
        viewDataBinding.imageViewBack.setOnClickListener(View.OnClickListener {
            goMainActivity()
        })    }

    fun goMainActivity() {
        finish()
    }

    private fun subscribeData() {
        recommendViewModel.apply {
            tripList.observe(this@RecommendTripActivity, Observer {
                it?.let {
                    adapter!!.setData(it)
                }
            })
        }
    }

    private fun setList() {
        val linearLayoutManager = LinearLayoutManager(this)
        viewDataBinding.recyclerView.layoutManager = linearLayoutManager

        adapter = TripAdapter(arrayListOf(), object : TripAdapter.OnItemClickListener {
            override fun onItemClick(item: Trip) {
                goRecommendedDetail(item)
            }
        })
        viewDataBinding.recyclerView.adapter = adapter
    }

    fun goRecommendedDetail(trip: Trip) {
        startActivity(intentFor<RecommendTripDetailActivity>("trip" to trip))
    }

    private fun obtainViewModel(): RecommendViewModel = obtainViewModel(RecommendViewModel::class.java)

}
