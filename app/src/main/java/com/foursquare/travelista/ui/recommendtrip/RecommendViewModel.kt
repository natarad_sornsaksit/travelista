/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foursquare.travelista.ui.recommendtrip

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import android.util.Log
import com.foursquare.travelista.SingleLiveEvent
import com.foursquare.travelista.data.Place
import com.foursquare.travelista.data.Plan
import com.foursquare.travelista.data.Trip
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import java.text.DecimalFormat


class RecommendViewModel(
        context: Application
) : AndroidViewModel(context) {

    val tripList = MutableLiveData<List<Trip>>()
    val placeList = MutableLiveData<List<Place>>()
    val planList = MutableLiveData<List<Plan>>()
    val isCompleted = SingleLiveEvent<Boolean>()

    val placeInPlan = MutableLiveData<List<Place>>()

    val trip = MutableLiveData<Trip>()

    val budget = ObservableField<String>("")

    fun syncData() {
        Log.i("syncData", "run")
        FirebaseFirestore.getInstance()
                .collection("Trips")
                .get()
                .addOnCompleteListener {
                    if (it.isComplete) {
                        val trips = mutableListOf<Trip>()
                        it.result.forEach {
                            val trip = it.toObject(Trip::class.java)
                            Log.i("trip", it.data.toString())
                            trips.add(trip)
                        }

                        if (trips.isNotEmpty()) {
                            tripList.postValue(trips)
                        }

                    } else {
                        Log.e("isComplete ${it.isComplete}", it.exception.toString())
                    }
                }
                .addOnFailureListener {
                    Log.e("syncData", it.toString())
                }
    }

    fun sync(trip: Trip) {
        Tasks.whenAll(findDetail(trip), syncPlace())
                .addOnFailureListener {

                }
                .addOnCompleteListener {
                    if (it.isComplete) {
                        isCompleted.postValue(true)
                        calculateBudget()
                    }
                }

    }

    private fun calculateBudget() {
        var calBudget: Int = 0
        val placeInPlan: MutableList<Place> = mutableListOf()
        planList.value?.forEach { plan: Plan ->
            val placeFind = placeList.value?.find {
                it.id == plan.place
            }
            placeFind?.let {
                placeInPlan.add(it)
                calBudget += it.budget
            }
        }

        this.placeInPlan.postValue(placeInPlan)

        val formatter = DecimalFormat("#,###,###")
        val yourFormattedString = formatter.format(calBudget)
        budget.set("Budget $yourFormattedString Baht")
    }

    fun findDetail(trip: Trip): com.google.android.gms.tasks.Task<QuerySnapshot> {
        return FirebaseFirestore.getInstance()
                .collection("Plans")
                .document(trip.planId)
                .collection("Activity")
                .orderBy("ArrivedTime", Query.Direction.ASCENDING)
                .get()
                .addOnCompleteListener {
                    val plans = mutableListOf<Plan>()
                    if (it.isComplete) {
                        it.result.forEach {
                            val planDetail = it.toObject(Plan::class.java)
                            plans.add(planDetail)
                            Log.i("findDetail", planDetail.toString())
                        }
                        if (plans.isNotEmpty()) {
                            planList.value = plans
                        }
                    }
                }.addOnFailureListener {
                    Log.e("findDetail", it.toString())
                }
    }


    fun syncPlace(): com.google.android.gms.tasks.Task<QuerySnapshot> {
        return FirebaseFirestore.getInstance()
                .collection("Places")
                .get()
                .addOnFailureListener {

                }
                .addOnCompleteListener {
                    val places = mutableListOf<Place>()

                    if (it.isComplete) {
                        it.result.forEach {
                            val planDetail = it.toObject(Place::class.java)
                            Log.i("findDetail", planDetail.toString())
                            places.add(planDetail.apply { ->
                                this.id = it.id
                            })
                        }
                    }

                    if (places.isNotEmpty()) {
                        placeList.value = places
                    }
                }
    }


}
