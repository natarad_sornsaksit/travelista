package com.foursquare.travelista.ui.recommendtrip

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.foursquare.travelista.R
import com.foursquare.travelista.data.Trip
import com.foursquare.travelista.databinding.ItemTripBinding


class TripAdapter(var data: ArrayList<Trip>, var listener: OnItemClickListener) : RecyclerView.Adapter<TripAdapter.WeatherViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: Trip)
    }


    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.bind(data[position])
    }

    fun setData(tripList: List<Trip>) {
        this.data = ArrayList(tripList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemTripBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_trip, parent, false)

        return WeatherViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size


    inner class WeatherViewHolder(val binding: ItemTripBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Trip) {
            binding.trip = data
            binding.executePendingBindings()
            itemView.setOnClickListener {
                listener.onItemClick(data)
            }
            Glide.with(itemView)
                    .load(data.background)
                    .into(binding.ivThumbnail)
        }
    }


}

