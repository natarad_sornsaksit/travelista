package com.foursquare.travelista.ui.signin

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.foursquare.travelista.R
import com.foursquare.travelista.databinding.ActivitySignInBinding
import com.foursquare.travelista.ui.signup.SignUpActivity
import com.foursquare.travelista.util.obtainViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.toast


class SignInActivity : AppCompatActivity() {

    private lateinit var signInViewModel: SignInViewModel
    private lateinit var viewDataBinding: ActivitySignInBinding
    private var mAuth: FirebaseAuth? = null
    private var TAG = "SignInActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in)
        signInViewModel = obtainViewModel()
        viewDataBinding.apply { viewmodel = signInViewModel }

        initInstance()

    }

    private fun initInstance() {

        viewDataBinding.ivBackHome.setOnClickListener(View.OnClickListener { view -> finish() })
        viewDataBinding.btCreate.setOnClickListener(View.OnClickListener { view -> onClick(view) })

        viewDataBinding.btLogin.setOnClickListener(View.OnClickListener { view -> signIn() })

    }

    private fun signIn() {

        var password = viewDataBinding.etPassword.text
        var email = viewDataBinding.etUsername.text

        if (!email.isEmpty()||password.isEmpty()) {

            mAuth!!.signInWithEmailAndPassword(email.toString(), password.toString())
                    .addOnCompleteListener(this, object : OnCompleteListener<AuthResult> {
                        override fun onComplete(task: Task<AuthResult>) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "signInWithEmail:success")
                                val user = mAuth!!.getCurrentUser()
                                //updateUI(user)
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "signInWithEmail:failure", task.getException())
                                Toast.makeText(this@SignInActivity, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show()
                                //updateUI(null)
                            }

                        }
                    })

        }else toast("PLEASE INPUT DATA!!")

    }

    private fun onClick(view: View?) {
        if (view === viewDataBinding.btCreate)

        {

            val intent = Intent(applicationContext, SignUpActivity::class.java)
            // val message = editText_main!!.text.toString()
            // intent.putExtra(EXTRA_MESSAGE, message)
            startActivity(intent)

        }



    }



    private fun obtainViewModel(): SignInViewModel = obtainViewModel(SignInViewModel::class.java)
}
