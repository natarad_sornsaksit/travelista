package com.foursquare.travelista.ui.signup

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.foursquare.travelista.R
import com.foursquare.travelista.databinding.ActivitySignUpBinding
import com.foursquare.travelista.util.obtainViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth


class SignUpActivity : AppCompatActivity() {



    private lateinit var signUpViewModel: SignUpViewModel
    private lateinit var viewDataBinding: ActivitySignUpBinding
    private var mAuth: FirebaseAuth? = null
    private val TAG = "SignUpActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        viewDataBinding = DataBindingUtil.setContentView(this,R.layout.activity_sign_up)
        signUpViewModel = obtainViewModel()
        viewDataBinding.apply { viewmodel = signUpViewModel }

        viewDataBinding.ivBackHome.setOnClickListener(View.OnClickListener { view -> finish() })

        initInstance()
    }

    private fun initInstance() {

//        var username = viewDataBinding.etUsername.text
//        var password = viewDataBinding.etPassword.text
//        var email = viewDataBinding.etMail.text

        viewDataBinding.btSignUp.setOnClickListener(View.OnClickListener { v: View? -> createAccount() } )

    }

    private fun createAccount() {

        var password = viewDataBinding.etPassword.text
        var email = viewDataBinding.etMail.text

        mAuth!!.createUserWithEmailAndPassword(email.toString(), password.toString())
                .addOnCompleteListener(this, object : OnCompleteListener<AuthResult> {
                    override fun onComplete(task: Task<AuthResult>) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success")
                            val user = mAuth!!.currentUser
                            //updateUI(user)
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException())
                            Toast.makeText(this@SignUpActivity, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show()
                            //updateUI(null)
                        }

                    }
                })

    }

    private fun obtainViewModel(): SignUpViewModel = obtainViewModel(SignUpViewModel::class.java)


    override fun onStart() {
        super.onStart()
        val currentUser = mAuth?.getCurrentUser()
        //updateUI(currentUser)
    }




}
