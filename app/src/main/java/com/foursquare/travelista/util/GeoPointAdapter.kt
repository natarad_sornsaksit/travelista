package com.foursquare.travelista.util

import android.os.Parcel
import com.google.firebase.firestore.GeoPoint
import paperparcel.TypeAdapter

object GeoPointAdapter : TypeAdapter<@JvmSuppressWildcards GeoPoint> {

    override fun readFromParcel(source: Parcel): GeoPoint {
        val doubleArray: DoubleArray = source.createDoubleArray()
        return GeoPoint(doubleArray[0], doubleArray[1])
    }


    override fun writeToParcel(value: GeoPoint?, dest: Parcel, flags: Int) {
        var doubleArray: DoubleArray = doubleArrayOf(0.0, 0.0)
        value?.let {
            doubleArray = doubleArrayOf(it.latitude, it.longitude)
        }
        dest.writeDoubleArray(doubleArray)
    }
}