package com.foursquare.travelista.util

import paperparcel.Adapter
import paperparcel.ProcessorConfig

@ProcessorConfig(adapters = [(Adapter(GeoPointAdapter::class))])
interface PaperParcelConfig